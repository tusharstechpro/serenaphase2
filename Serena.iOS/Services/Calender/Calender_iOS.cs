﻿using EventKit;
using Foundation;
using Serena.Interface;
using Serena.iOS;
using Serena.iOS.Services.Calender;
using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;


[assembly: Dependency(typeof(Calender_iOS))]
namespace Serena.iOS.Services.Calender
{
    public class Calender_iOS : ICalenderDroid
    {

        public void AddEventIniOS(List<ScheduleDateTime> SDT)
        {
            EKEvent newEvent = EKEvent.FromStore(EventClass.Current.EventStore);
            DateTime a = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 00);
            DateTime b = new DateTime(SDT[0].Year, SDT[0].Month, SDT[0].Date, SDT[0].Hour, SDT[0].Minute, 00);
            double timeDiff = b.Subtract(a).TotalMinutes;
            newEvent.StartDate = (NSDate)DateTime.Now.AddMinutes(timeDiff);
            double EndTime = timeDiff + 60;
            newEvent.EndDate = (NSDate)DateTime.Now.AddMinutes(EndTime);
            //event tittle
            newEvent.Title = SDT[0].Title;
            newEvent.Calendar = EventClass.Current.EventStore.DefaultCalendarForNewEvents;
            //save Event
            NSError e;
            EventClass.Current.EventStore.SaveEvent(newEvent, EKSpan.ThisEvent, true, out e);

        }
        public void RID()
        { }

        public void AddEventInAndroid(List<ScheduleDateTime> SDT)
        { }
    }
}