﻿using Foundation;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Syncfusion.ListView.XForms.iOS;
using Syncfusion.SfBusyIndicator.XForms.iOS;
using Syncfusion.SfPicker.XForms.iOS;
using UIKit;
using System;
using System.Threading.Tasks;
using Matcha.BackgroundService.iOS;
using UserNotifications;
using Serena.iOS.LocalNotification;


namespace Serena.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public static string GoogleiOSClientId = "117882128924-ffmuvmn5lt09t0mhm6q59nsipjncqa6v.apps.googleusercontent.com";
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            // code added by amd to handle app crash
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
            // app crash handling code finished 

            //syncfusion key
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjgxNzQyQDMxMzgyZTMyMmUzMG9tSFhmYjVqak9DNzlyQnZpNjBVVHJkT1FXdURDdlhsMTIyQVQ4Rk1CQU09");
            global::Xamarin.Forms.Forms.Init();
            global::Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();
            Xamarin.FormsMaps.Init();
            Xam.Forms.VideoPlayer.iOS.VideoPlayerRenderer.Init();

            // set a delegate to handle incoming notifications
            UNUserNotificationCenter.Current.Delegate = new iOSNotificationReceiver();

            #region syncfusion tools
            //new Syncfusion.SfAutoComplete.XForms.iOS.SfAutoCompleteRenderer();
            new SfBusyIndicatorRenderer();
            SfListViewRenderer.Init();
            //new Syncfusion.SfNavigationDrawer.XForms.iOS.SfNavigationDrawerRenderer();
            //Syncfusion.XForms.iOS.PopupLayout.SfPopupLayoutRenderer.Init();
            Syncfusion.XForms.iOS.TabView.SfTabViewRenderer.Init();
            Syncfusion.XForms.iOS.Buttons.SfButtonRenderer.Init();
            //new Syncfusion.XForms.iOS.ComboBox.SfComboBoxRenderer();
            Syncfusion.XForms.iOS.Border.SfBorderRenderer.Init();
            Syncfusion.SfRating.XForms.iOS.SfRatingRenderer.Init();
            Syncfusion.XForms.iOS.Buttons.SfSegmentedControlRenderer.Init();
            Syncfusion.XForms.iOS.Buttons.SfSwitchRenderer.Init();
            SfPickerRenderer.Init();
            #endregion

            try
            {
                LoadApplication(new App());
                FacebookClientManager.Initialize(app, options);
                GoogleClientManager.Initialize(GoogleiOSClientId);

                // background service

                BackgroundAggregator.Init(this);

                return base.FinishedLaunching(app, options);
            }
            catch (ApplicationException ex)
            {
                return false;
            }
        }

        // code added my amd to handle app crash

        private static void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
        {
            var newExc = new Exception("TaskSchedulerOnUnobservedTaskException", unobservedTaskExceptionEventArgs.Exception);

        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var newExc = new Exception("CurrentDomainOnUnhandledException", unhandledExceptionEventArgs.ExceptionObject as Exception);

        }
        // code finished to handle app crash


        public override void OnActivated(UIApplication uiApplication)
        {
            base.OnActivated(uiApplication);
            FacebookClientManager.OnActivated();
        }
        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            GoogleClientManager.OnOpenUrl(app, url, options);
            return FacebookClientManager.OpenUrl(app, url, options);
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            return FacebookClientManager.OpenUrl(application, url, sourceApplication, annotation);
        }
    }
}
