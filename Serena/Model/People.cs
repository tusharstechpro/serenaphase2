﻿using SQLite;

namespace Serena.Model
{
    public class People
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserName { get; set; }
        public string UserProfileImage { get; set; }
        public double AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Topics { get; set; }
    }
    public class ProMembership
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Description { get; set; }
        public string BDescription { get; set; }
        public string PDescription { get; set; }
        public string PDescription1 { get; set; }
        public string PDescription2 { get; set; }
        public string PDescription3 { get; set; }
        public string PDescription4 { get; set; }
        public string PDescription5 { get; set; }
        public string PDescription6 { get; set; }
    }
}
