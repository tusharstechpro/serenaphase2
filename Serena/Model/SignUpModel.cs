﻿using SQLite;
//using Newtonsoft.Json.Serialization;
using System;

namespace Serena.Model
{
    public class SignUpModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        //[Required]
        public string FULLNAME { get; set; }
        //[Required, MaxLength(20), EmailAddress]
        public string EMAIL { get; set; }
        public DateTime LastCreated { get; set; }
        public string LastLoginTime { get; set; }
        public DateTime LastUpdated { get; set; }
        //[Required,MaxLength(10)]
        public string PASSWORD { get; set; }
        //[Required]
        public string PLACE { get; set; }
        public string UserCategory { get; set; }
        public string UserLoginType { get; set; }
        //[Required]
        public string USERNAME { get; set; }
        //public string UserStatus { get; set; }
    }

    public class SignUpModel_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string FULLNAME { get; set; }
        public string EMAIL { get; set; }
        public DateTime LastCreated { get; set; }
        public string LastLoginTime { get; set; }
        public DateTime LastUpdated { get; set; }
        public string PASSWORD { get; set; }
        public string PLACE { get; set; }
        public string UserCategory { get; set; }
        public string UserLoginType { get; set; }
        public string USERNAME { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string Status { get; set; }//0-Save, 1-Edit

    }
}
