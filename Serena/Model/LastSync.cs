﻿using System;
using SQLite;

namespace Serena.Model
{
    public class LastSync
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string TableName { get; set; }
        public DateTime Out_last_sync { get; set; }
        public DateTime Get_last_sync { get; set; }
    }
}
