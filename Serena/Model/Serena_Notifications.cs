﻿using SQLite;
using System;

namespace Serena.Model
{
    public class Serena_Notifications
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string CountORRequestBy { get; set; }
        public string Guid { get; set; }
        public string ImageUrl { get; set; }
        public string PostText { get; set; }
        public string PostColor { get; set; }
        public string BackgroundColor { get; set; }
        public string PostType { get; set; }
        public string category { get; set; }
        public string UserEmailId { get; set; }
        public string NotificationLbl { get; set; }
        public string LastUpdated { get; set; }
    }
    public class Upcoming_Notifications
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string ClassGuid { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public string WatchPartyGuid { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string ScheduledTime { get; set; }
        public string AMPM { get; set; }
        public string HostEmailID { get; set; }
        public string Accepted { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Members { get; set; }
        public string ButtonText { get; set; }
        public string Scheduleday { get; set; }
    }
}
