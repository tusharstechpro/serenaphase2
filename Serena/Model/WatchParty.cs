﻿using SQLite;
using System;
using System.ComponentModel;

namespace Serena.Model
{
    public class WatchParty
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string ClassGuid { get; set; }//video GUID
        public DateTime ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string AMPM { get; set; }
        public string MarkToCalendar { get; set; }
        public string RemindViaApp { get; set; }
        public string RemindViaTxt { get; set; }
    }

    public class WatchParty_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string ClassGuid { get; set; }//video GUID
        public DateTime ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string AMPM { get; set; }
        public string MarkToCalendar { get; set; }
        public string RemindViaApp { get; set; }
        public string RemindViaTxt { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string WPApiStatus { get; set; }//0-Create WP 1-Delete WP 2-Edit WP
    }
    public class WatchPartyTran
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string WatchPartyGuid { get; set; }
        public string Members { get; set; }// UserEmailId 
        public string Accepted { get; set; }// added on 22march2021 by shraddhap 
    }

    public class WatchPartyTran_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string WatchPartyGuid { get; set; }
        public string Members { get; set; }// UserEmailId 
        public DateTime LastSyncDate { get; set; }
        public string Accepted { get; set; }// added on 22march2021 by shraddhap 

    }
    public class WatchPartyList
    {
        public string UserName { get; set; }
        public string ProfileImageUrl { get; set; }
        public string ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        // UserEmailId 
    }
}
