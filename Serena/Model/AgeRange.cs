﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Serena.Model
{
	[Table("Serena_AgeRangeMaster")]
	public class AgeRange
	{
		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }
		public string AgeRangeName { get; set; }

	}
}