﻿using SQLite;

namespace Serena.Model
{
    public class ExceptionLog
    {
        //ID/ExceptionDetails/ ExceptionCategoryScreenName/OSType/DeviceInfo/TimeStamp
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string ExceptionDetails { get; set; }
        public string ExceptionCategoryScreenName { get; set; }
        public string ExceptionEventName { get; set; }
        public string OSType { get; set; }//Device Platform
        public string DeviceModel { get; set; }//Name of device
        public string DeviceVersion { get; set; }//Operating System Version Number  
        public string ErrorLogTime { get; set; }
    }
}
