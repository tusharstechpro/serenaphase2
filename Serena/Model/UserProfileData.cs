﻿using SQLite;
using System;
using System.ComponentModel;
namespace Serena.Model
{
    [Table("Serena_UserProfileData")]
    public class UserProfileData
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserName { get; set; }
        //Social Profile Fields
        [DefaultValue(0)]
        public int IsPro { get; set; }
        [DefaultValue("NO")]
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public DateTime LastUpdated { get; set; }
    }

    public class UserProfileData_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserName { get; set; }
        //Social Profile Fields
        [DefaultValue(0)]
        public int IsPro { get; set; }
        [DefaultValue("NO")]
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string Status { get; set; }//0-save, 1-Edit
    }
    public class ProMemberToolsModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string Categories { get; set; }
        public string Tagline { get; set; }
        public string About { get; set; }
        public string Coverphoto { get; set; }
    }

    [Table("Serena_UserProfileData_Tran")]
    public class UserProfileData_Tran
    {
        [AutoIncrement, PrimaryKey]
        public int UserTranId { get; set; }
        public string UserEmailId { get; set; }
        public string RequestTo { get; set; }
        public int Status { get; set; }
        public string Guid { get; set; }
        public DateTime LastUpdated { get; set; }
    }

    [Table("Serena_UserProfileData_TranOut")]
    public class UserProfileDataTran_Out
    {
        [AutoIncrement, PrimaryKey]
        public int UserTranOutId { get; set; }
        public string UserEmailId { get; set; }
        public string RequestTo { get; set; }
        public int Status { get; set; }
        public string Guid { get; set; }
        public DateTime LastSyncDate { get; set; }
    }

    public class UploadMediaModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
    }

    public class UploadMediaModel_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public DateTime LastSyncDate { get; set; }
    }
    public class FindUploadClassModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public double AvgRating { get; set; }
        public string TotalReviews { get; set; }

    }
    public class SearchClassModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Mentions { get; set; }
    }
}
