﻿using Serena.DBService.SocialMediaIntegration;
using Serena.Model;
using Serena.ViewModel.SocialAndroid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        IOAuth2Service oAuth2Service;
        public SignUpPage()
        {
            InitializeComponent();
            try
            {
                this.BindingContext = new SocialSignuppageViewModel(oAuth2Service);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SignUpPage.xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}