﻿using Syncfusion.XForms.Buttons;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.DBService;
using Serena.Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Helpers;
using Syncfusion.SfBusyIndicator.XForms;
using System.Text.RegularExpressions;
using static Serena.Model.GooglePlaces.Address;
using static Serena.Helpers.Constants;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpEmail : ContentPage
    {
        string LoginTypes;
        SignUpModel signup = new SignUpModel();
        SignUpModel_Out signup_out = new SignUpModel_Out();
        UserProfileData profileData = new UserProfileData();
        UserProfileData_Out profileData_out = new UserProfileData_Out();
        UserProfileData_Tran profileDataTran = new UserProfileData_Tran();
        CryptographyASE Crypto = new CryptographyASE();
        public string SwitchValue;

        //variables added for deafult follow test456
        string IsPro = "0";
        string sessionEmailId;
        public int _pinCreatedCount = 0;
        public string ResponseData;
        public SignUpEmail(string Email, string Name, string LoginType)
        {
            InitializeComponent();
            Txtemail.Text = Email;
            Txtfullname.Text = Name;
            Txtusername.Text = Email;
            LoginTypes = LoginType;
            //Password field readonly when UserLoginType is not equal to 0
            if (LoginType == "0")
            {
                Txtpassword.IsReadOnly = false;
                TxtCpassword.IsReadOnly = false;
            }
            else
            {
                Txtpassword.IsReadOnly = true;
                TxtCpassword.IsReadOnly = true;
            }
        }

        //SignUp Button Click
        async void SignUpClicked(object sender, EventArgs e)
        {
            try
            {

                var current = Connectivity.NetworkAccess;

                // Connection to internet is available
                if (current == NetworkAccess.Internet)
                {
                    //If any required field is empty or null then highlight the border
                    if (string.IsNullOrEmpty(Txtemail.Text) || string.IsNullOrEmpty(Txtfullname.Text) || string.IsNullOrEmpty(Txtusername.Text) || string.IsNullOrEmpty(Txtpassword.Text) || string.IsNullOrEmpty(TxtCpassword.Text) || string.IsNullOrEmpty(Txtplace.Text))
                    {
                        if (string.IsNullOrEmpty(Txtemail.Text))
                        {
                            input1.HasError = true;
                        }
                        else
                        {
                            input1.HasError = false;
                            input1.ErrorText = "";
                        }
                        if (string.IsNullOrEmpty(Txtfullname.Text))
                        {
                            input2.HasError = true;
                        }
                        else
                        {
                            input2.HasError = false;
                            input2.ErrorText = "";
                        }
                        if (string.IsNullOrEmpty(Txtusername.Text))
                        {
                            input3.HasError = true;
                        }
                        else
                        {
                            input3.HasError = false;
                            input3.ErrorText = "";
                        }
                        if (LoginTypes == "0")
                        {
                            if (string.IsNullOrEmpty(Txtpassword.Text))
                            {
                                input4.HasError = true;
                            }

                            if (string.IsNullOrEmpty(TxtCpassword.Text))
                            {
                                input5.HasError = true;
                            }
                            else
                            {
                                input5.HasError = false;
                                input5.ErrorText = "";
                            }
                        }
                        else
                        {
                            Txtpassword.Text = "testpass@@";
                            TxtCpassword.Text = "testpass@@";
                        }
                        if (string.IsNullOrEmpty(Txtplace.Text))
                        {
                            input6.HasError = true;
                        }
                        else
                        {
                            input6.HasError = false;
                            input6.ErrorText = "";
                        }
                    }

                    //if all required data is entered
                    if (!string.IsNullOrEmpty(Txtemail.Text) && !string.IsNullOrEmpty(Txtfullname.Text) && !string.IsNullOrEmpty(Txtusername.Text) && !string.IsNullOrEmpty(Txtpassword.Text) && !string.IsNullOrEmpty(TxtCpassword.Text) && !string.IsNullOrEmpty(Txtplace.Text))
                    {
                        //To apply loading... as a busy indicator
                        SfBusyIndicator busyIndicator = new SfBusyIndicator()
                        {
                            AnimationType = AnimationTypes.Cupertino,
                            ViewBoxHeight = 50,
                            ViewBoxWidth = 50,
                            EnableAnimation = true,
                            Title = "Loading...",
                            TextSize = 12,
                            FontFamily = "Cabin",
                            TextColor = Color.FromHex("#9900CC")
                        };
                        this.Content = busyIndicator;


                        input4.HasError = false;
                        input4.ErrorText = "";
                        input3.HasError = false;
                        input3.ErrorText = "";

                        // Getspecific user data from SQLite to check Email is already exists
                        var sourcedata = App.DBIni.SignUpDB.GetSpecificUserByEmail(Txtemail.Text);
                        if (sourcedata == null)
                        {
                            //password encryption
                            string encPassword = Crypto.Encrypt(Txtpassword.Text);

                            #region add data serena user table in SQLite
                            signup_out.FULLNAME = signup.FULLNAME = Txtfullname.Text;
                            signup_out.EMAIL = signup.EMAIL = Txtemail.Text;
                            signup_out.LastCreated = signup.LastCreated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                            signup_out.LastLoginTime = signup.LastLoginTime = DateTime.Now.ToString("h:mm tt");
                            signup_out.LastUpdated = signup.LastUpdated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                            signup_out.PASSWORD = signup.PASSWORD = encPassword;
                            signup_out.PLACE = signup.PLACE = Txtplace.Text;
                            signup_out.UserCategory = signup.UserCategory = SwitchValue;
                            signup_out.UserLoginType = signup.UserLoginType = LoginTypes;
                            signup_out.USERNAME = signup.USERNAME = Txtusername.Text;
                            signup_out.LastSyncDate = DateTime.Now;
                            signup_out.Status = "0";//Save

                            var retrunvalue = App.DBIni.SignUpDB.AddUser(signup);

                            //adding in out table
                            var retturnvalue_Out = App.DBIni.SignUpDB.AddUser_out(signup_out);

                            #endregion

                            #region add data in Serena_User table in MySql
                            //var serenaDataSync = new SerenaDataSync();

                            //// Connection to internet is available
                            //if (current == NetworkAccess.Internet)
                            //{
                            //    await serenaDataSync.SaveSerenaUserData(signup);
                            //}
                            //else
                            //    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                            #endregion

                            #region add data in UserProfileData table in SQLite

                            var userProfileInfo = App.DBIni.UserProfileDataDB.GetSpecificUser(Txtemail.Text);
                            if (userProfileInfo == null)
                            {
                                profileData_out.UserAccountType = profileData.UserAccountType = "PUBLIC";
                                profileData_out.UserAgeRange = profileData.UserAgeRange = "-";
                                profileData_out.UserBio = profileData.UserBio = "-";
                                profileData_out.UserEmailId = profileData.UserEmailId = Txtemail.Text;
                                profileData_out.UserLocation = profileData.UserLocation = "-";
                                profileData_out.UserPostJson = profileData.UserPostJson = "-";
                                profileData_out.UserProfileImage = profileData.UserProfileImage = "-";
                                profileData_out.UserName = profileData.UserName = Txtusername.Text;
                                profileData_out.UserWebsite = profileData.UserWebsite = "-";
                                profileData_out.Status = "0";
                                profileData_out.LastSyncDate = DateTime.Now;

                                App.DBIni.UserProfileDataDB.AddUser(profileData);

                                //out table entry
                                App.DBIni.UserProfileDataDB.AddUserOut(profileData_out);

                            }
                            #endregion

                            #region add data in userProfile table in Mysql

                            // Connection to internet is available
                            //if (current == NetworkAccess.Internet)
                            //{
                            //    await serenaDataSync.SaveSerenaUserProfileData(profileData);
                            //}
                            //else
                            //    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                            #endregion

                            #region add by default user to follow test456 in SQLite and MySql
                            //string RequestUserEmail = "test456@gmail.com";
                            //Guid guid = Guid.NewGuid();
                            //string Guidstr = guid.ToString();
                            //Guidstr = (guid + RequestUserEmail);
                            //if (current == NetworkAccess.Internet)
                            //{
                            //    var serenaDataSync = new SerenaDataSync();
                            //    await serenaDataSync.InsertUserProfileTran(sessionEmailId, RequestUserEmail, Guidstr, "1");

                            //    #region add data in UserProfile table in SQLite                 
                            //    profileDataTran.UserEmailId = sessionEmailId;
                            //    profileDataTran.RequestTo = RequestUserEmail;
                            //    profileDataTran.Status = 1;
                            //    profileDataTran.Guid = Guidstr;
                            //    App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                            //    #endregion
                            //}
                            //else
                            //    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                            #endregion

                            //add user to session
                            Settings.GeneralSettings =
                            Settings.GeneralSettings = Txtemail.Text;
                            //navigate to ToadyPage
                            //await App.Current.MainPage.Navigation.PushModalAsync(new SerenaTabbedPage());
                            await (App.Current.MainPage as Xamarin.Forms.Shell).GoToAsync("//tabbar/tabRouteFind/shellRouteFind", true);
                        }
                        else///to avoid double tabbed
                        {
                            bool answer = await DisplayAlert("", "Email already exist!", "OK", "Cancel");
                            if (answer == true)
                            {
                                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail("", "", "0"));
                            }
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SignUpEmail.Xaml";
                addlogFile.ExceptionEventName = "SignUpClicked";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Login Button Click
        async void LogInClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (LoginTypes != "0")
                {
                    await Navigation.PushAsync(new LogInUsers());
                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        //To check whether password and confirm password is same 
        private void ConfirmPassEvent(object sender, EventArgs e)
        {
            if (Txtpassword.Text != TxtCpassword.Text)
            {
                input5.HasError = true;
                input5.ErrorText = "Password and Confirm Password are not same! ";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                input5.HasError = false;
                input5.ErrorText = "";
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = true;
            }

            if (input1.HasError == true || input4.HasError == true || input5.HasError == true)
            {
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = true;
            }
        }

        //For password validation
        private void PassEvent(object sender, EventArgs e)
        {
            var input = Txtpassword.Text;
            var test = new Regex(@"^(?=.*[a-z])(?=.*[@$!%*?&#_()-+/!])[A-Za-z\d@$!%*?&#_()-+/!]{8,10}$");
            var isValidated = test.IsMatch(input);

            if (isValidated == false)
            {
                input4.HasError = true;
                input4.ErrorText = "Password must contain 8-10 characters with symbol (!, &, *, etc)!";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                input4.HasError = false;
                input4.ErrorText = "";
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = true;
            }
            if (Txtpassword.Text != TxtCpassword.Text)
            {
                input5.HasError = true;
                input5.ErrorText = "Password and Confirm Password are not same! ";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                input5.HasError = false;
                input5.ErrorText = "";
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = true;
            }
            if (input1.HasError == true || input4.HasError == true || input5.HasError == true)
            {
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = true;
            }
        }


        //Swich change operation 
        //private void SwitchChanged(object sender, EventArgs e)
        //{
        //    if (Switch1.IsFocused == true)
        //    {
        //        SwitchValue = "Business";
        //    }
        //    else
        //    {
        //        SwitchValue = "Personal";
        //    }

        //}

        private void EmailTextChangeEvent(object sender, TextChangedEventArgs e)
        {
            var emailInput = Txtemail.Text;
            if (string.IsNullOrEmpty(emailInput))
            {
                input1.HasError = false;
                input1.ErrorText = "";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                string pattern = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

                //check first string
                if (!Regex.IsMatch(emailInput, pattern))
                {
                    input1.HasError = true;
                    input1.ErrorText = "Please enter valid email address";
                    SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = false;
                }
                else
                {
                    input1.HasError = false;
                    input1.ErrorText = "";
                    SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = true;
                }
                if (input1.HasError == true || input4.HasError == true || input5.HasError == true)
                {
                    SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = false;
                }
                else
                {
                    SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = true;
                }
            }
        }

        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (!string.IsNullOrEmpty(Txtplace.Text))
                    {
                        //list is visible false
                        AddressList.IsVisible = true;
                        await GetPlacesPredictionsAsync();
                        AddressList.ItemsSource = Addresses;

                    }
                    else if (string.IsNullOrEmpty(Txtplace.Text))
                    {
                        //list is visible false
                        AddressList.IsVisible = false;
                        AddressList.ItemsSource = null;
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {

            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time

            try
            {
                CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(Txtplace.Text))))
                { //Be sure to UrlEncode the search term they enter

                    using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                            PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                            if (predictionList.Status == "OK")
                            {
                                Addresses.Clear();

                                if (predictionList.Predictions.Count > 0)
                                {
                                    foreach (Prediction prediction in predictionList.Predictions)
                                    {
                                        Addresses.Add(new AddressInfo
                                        {
                                            Address = prediction.Description
                                        });
                                    }
                                }
                            }
                            else
                            {
                                AddressList.ItemsSource = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                AddressList.ItemsSource = "";
                var PlaceAddress = (Xamarin.Forms.Label)sender;
                Txtplace.Text = PlaceAddress.Text;
                //list isible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}