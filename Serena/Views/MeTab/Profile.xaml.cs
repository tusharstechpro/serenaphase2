﻿using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.MeTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile : ContentPage
    {
        string sessionEmailId;
        public Profile()
        {
            InitializeComponent();
            sessionEmailId = Settings.GeneralSettings;
            if (!string.IsNullOrEmpty(sessionEmailId))
            {
                logoutbtn.Text = "SIGN OUT";
            }
            else if(string.IsNullOrEmpty(sessionEmailId))
            {
                logoutbtn.Text = "SIGN IN";
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                sessionEmailId = Settings.GeneralSettings;
                if (!string.IsNullOrEmpty(sessionEmailId))
                {
                    logoutbtn.Text = "SIGN OUT";
                }
                else if (string.IsNullOrEmpty(sessionEmailId))
                {
                    logoutbtn.Text = "SIGN IN";
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "OnAppearing";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        private async void Button_Clicked(object sender, EventArgs e)
        {
            //Clear the session before logout
            Settings.GeneralSettings = "";
            await Navigation.PushModalAsync(new NavigationPage(new SignInPage()));
        }
    }
}