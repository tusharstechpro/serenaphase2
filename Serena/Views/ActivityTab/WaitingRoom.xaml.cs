﻿using Plugin.Toast;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xam.Forms.VideoPlayer;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WaitingRoom : ContentPage
    {
        string watchPartyGuId, sessionEmailId;
        public WaitingRoom(string watchpartyguid)
        {
            InitializeComponent();

            LoadInitialData(watchpartyguid);
        }

        private void LoadInitialData(string watchpartyguid)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //Get the login user EmailId from Session
                    sessionEmailId = Settings.GeneralSettings;
                    watchPartyGuId = watchpartyguid;

                    var WatchPartyData = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(watchpartyguid);
                    if (WatchPartyData != null)
                    {
                        var HostDetails = App.DBIni.UserProfileDataDB.GetSpecificUser(WatchPartyData.HostEmailID);
                        if (HostDetails != null)
                        {
                            HostImg.Source = HostDetails.UserProfileImage;
                            string nos = WatchPartyData.Members;
                            if (nos != "")
                            {
                                WaitingList.IsVisible = true;
                                List<string> numbers = nos.Split(',').ToList<string>();
                                var watchPartylist = new List<WatchPartyList>();
                                foreach (var item in numbers)
                                {
                                    var emailid = item;
                                    var UserProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(emailid);
                                    WatchPartyList WPL = new WatchPartyList();
                                    WPL.UserName = "@" + UserProfileData.UserName;
                                    WPL.ProfileImageUrl = UserProfileData.UserProfileImage;
                                    watchPartylist.Add(WPL);
                                }
                                WaitingList.ItemsSource = watchPartylist;
                            }
                            else
                            {
                                WaitingList.IsVisible = false;
                            }
                        }

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "WaitingRoom.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void PlayBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var partyDetails = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(watchPartyGuId);
                if (partyDetails != null)
                {
                    DateTime scheduleDate = Convert.ToDateTime(partyDetails.ScheduledDate);
                    DateTime scheduleDate1 = scheduleDate.Date;
                    DateTime currentDate = DateTime.Now.Date;
                    TimeSpan Difference = scheduleDate1 - currentDate;
                    var Days = Difference.Days;

                    if (Days == 0)
                    {
                        var WatchPartyData = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(watchPartyGuId);
                        if (WatchPartyData != null)
                        {
                            UriVideoSource uriVideoSource = new UriVideoSource()
                            {
                                Uri = WatchPartyData.MediaUrl
                            };
                            videoPlayer.Source = uriVideoSource;
                        }
                    }
                    else
                    {
                        await DisplayAlert("", "Wait for watch party to start", "OK", "Cancel");
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "WaitingRoom.Xaml";
                addlogFile.ExceptionEventName = "PlayBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void OnLabel1Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ThanksPopupScreen());
            //await App.Current.MainPage.Navigation.PushModalAsync(new ThanksPopupScreen());
        }
    }
}