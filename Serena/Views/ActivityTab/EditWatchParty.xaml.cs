﻿using Plugin.Permissions;
using Plugin.Toast;
using Serena.DBService;
using Serena.Helpers;
using Serena.Interface;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditWatchParty : ContentPage
    {
        public string watchpartyGUID;
        SfBusyIndicator busyIndicator = new SfBusyIndicator();
        List<ScheduleDateTime> SDT = new List<ScheduleDateTime>();
        DateTime SHDate;
        string SHTime;
        WatchParty WP = new WatchParty();
        INotificationManager notificationManager;

        #region Datetime
        ObservableCollection<object> todaycollection = new ObservableCollection<object>();

        private ObservableCollection<object> _startdate;

        public ObservableCollection<object> StartDate
        {
            get { return _startdate; }
            set { _startdate = value; RaisePropertyChanged("StartDate"); }
        }

        void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public EditWatchParty(string WPGuid, DateTime SchDate, string SchTime, string format)
        {
            InitializeComponent();
            LoadInitialData(WPGuid, SchDate, SchTime,format);
        }

        private void LoadInitialData(string WPGuid, DateTime SchDate, string SchTime, string format)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    #region Set Schedule date to datetime picker as default
                    //To remove ":" from time string
                    string test = SchTime.Replace(":", "");
                    //To get hours from time string
                    string hr = test.Substring(0, 2);
                    //To get minutes from time string
                    string min  = test.Remove(0, 2);
                    //To add schedule year to datetime picker
                    todaycollection.Add(SchDate.Date.Year.ToString());
                    //To add schedule month to datetime picker
                    todaycollection.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(SchDate.Date.Month).Substring(0, 3));
                    //To add schedule date to datetime picker
                    if (SchDate.Date.Day < 10)
                        todaycollection.Add("0" + SchDate.Date.Day);
                    else
                        todaycollection.Add(SchDate.Date.Day.ToString());
                    //To add hours to datetime picker
                    todaycollection.Add(hr);
                    //To add minute to datetime picker
                    todaycollection.Add(min);
                    //To add format to datetime picker
                    todaycollection.Add(format);
                    this.StartDate = todaycollection;
                    date.SelectedItem = StartDate;
                    #endregion
                    //To get watchpartyGUID from prev page
                    watchpartyGUID = WPGuid;
                    //To get Scheduledate from prev page
                    SHDate = SchDate;
                    //To get Scheduletime from prev page
                    SHTime = SchTime;
                    //Delete watch party tran data before adding 
                    //App.DBIni.WatchPartyDB.DeleteAllWatchPartyTran();
                    //To get watchpartytran data from API
                    //SerenaDataSync serenaDataSync = new SerenaDataSync();
                    //Task.Run(() => serenaDataSync.GetWatchPartyDetails(watchpartyGUID)).Wait();
                    //To get members data from watchpartytran on watchpartyGUID
                    var membersData = App.DBIni.WatchPartyDB.GetWPMembersByWPGuId(watchpartyGUID);

                    if (membersData.Count() != 0)
                    {
                        var WPList = new List<WatchPartyList>();
                        foreach (var members in membersData)
                        {
                            //To get user profile data for image and username binding
                            var userprofiledata = App.DBIni.UserProfileDataDB.GetSpecificUser(members.Members);
                            if (userprofiledata != null)
                            {
                                WatchPartyList WP = new WatchPartyList();
                                WP.UserName = userprofiledata.UserName;
                                WP.ProfileImageUrl = userprofiledata.UserProfileImage;
                                WPList.Add(WP);
                            }
                        }
                        //To add data to Group Member list
                        GroupMemberList.ItemsSource = WPList;
                    }
                    //To pass current datetime to calender
                    SDT.Add(new ScheduleDateTime { Year = DateTime.Now.Year, Month = DateTime.Now.Month, Date = DateTime.Now.Day, Hour = DateTime.Now.Hour, Minute = DateTime.Now.Minute, Title = "Serena Watch Party" });
                    //To get month name from number of month
                    System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                    lblToday.Text = "Today," + " " + mfi.GetMonthName(SchDate.Month).ToString() + " " + SchDate.Day + ", " + SchTime + "   >";
                    #region Frame Date Binding 
                    var shDate1 = SHDate.Date.ToString("MMMM dd");
                    datelbl.Text = shDate1.Substring(shDate1.Length - 2, 2) + " " + shDate1.Substring(0, 3);
                    #endregion
                    //Notification service init
                    notificationManager = DependencyService.Get<INotificationManager>();

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "DoneWithClass.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void DeleteEvent_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    bool answer = await DisplayAlert("", "You really want to delete watch party?", "Yes", "No");
                    if (answer == true)
                    {
                        //To add Loading... on screen
                        SfBusyIndicator();
                        #region Delete WP from UpcomingNotification table SQLite
                        var returnvalue = App.DBIni.NotificationDB.DeleteByWPGuId(watchpartyGUID);
                        #endregion

                        #region Delete WP data from Watchparty table and WatchPartyTran Table SQLite
                        App.DBIni.WatchPartyDB.DeleteWPByGuId(watchpartyGUID);//Delete from WatchParty table
                        App.DBIni.WatchPartyDB.DeleteWPTByGuId(watchpartyGUID);//Delete from WatchPartyTran table
                        #endregion

                        #region Update IsDeleted status to WPOut table for delete the records
                        var WPByGuId = App.DBIni.WatchPartyDB.GetWPByWPGuId(watchpartyGUID);
                        if (WPByGuId != null)
                        {
                            WPByGuId.WPApiStatus = "1";//Delete
                            //adding in sqllite out table
                            App.DBIni.WatchPartyDB.AddWatchParty_Out(WPByGuId);
                        }

                        #endregion

                        await Navigation.PushAsync(new Activity());

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "EditWatchParty.Xaml";
                addlogFile.ExceptionEventName = "DeleteEvent_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void DONEBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var WPDetails = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(watchpartyGUID);
                    if (WPDetails != null)
                    {
                        #region To generate notification via app
                        if (reminderSwitch.IsOn == true)
                        {
                            //To get selected date from datepicker
                            var DatePickerValue = date.SelectedItem as ObservableCollection<object>; ;
                            //To get selected time format
                            string format = DatePickerValue[5].ToString();
                            string title = $"Join a " + WPDetails.Title + " class at " + SHTime + format + " today!";
                            string message = $"Open up your app to see details about this upcoming class and accept the invitation.";
                            //create a datetime value
                            var dates = DatePickerValue[0].ToString() + "/" + DatePickerValue[1].ToString() + "/" + DatePickerValue[2].ToString() + " " + DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString() + ":" + "00" + " " + format;
                            //To convert string to datetime
                            CultureInfo culture = new CultureInfo("en-US");
                            //To convert string to datetime
                            DateTime notifyDate = Convert.ToDateTime(dates, culture);
                            //To get 5 minutes prior datetime
                            DateTime notifyTime = notifyDate.AddMinutes(-5);
                            //To pass notification details to SendNotification
                            notificationManager.SendNotification(title, message, notifyTime);
                        }
                        #endregion

                        #region Update WP data in watchparty and out table
                        var WPByGuId = App.DBIni.WatchPartyDB.GetWPByWPGuId(watchpartyGUID);
                        if (WPByGuId != null)
                        {
                            WP.ScheduleDate = WPByGuId.ScheduleDate = SHDate;
                            WP.ScheduleTime = WPByGuId.ScheduleTime = SHTime;
                            WPByGuId.LastSyncDate = DateTime.Now;
                            WPByGuId.WPApiStatus = "2"; //Edit WP
                            //adding in sqllite
                            App.DBIni.WatchPartyDB.updateWatchParty(WP);
                            //adding in sqllite out table
                            App.DBIni.WatchPartyDB.AddWatchParty_Out(WPByGuId);
                        }
                        #endregion

                        #region Update WP data in UpcomingNotifications table
                        //To update watch party scheduledate and time in Sqlite
                        WPDetails.ScheduledDate = SHDate;
                        WPDetails.ScheduledTime = SHTime;
                        App.DBIni.NotificationDB.UpdateUpcomingNotifications(WPDetails);
                        #endregion
                    }
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "EditWatchParty.Xaml";
                addlogFile.ExceptionEventName = "DONEBtn_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        private async void calendarSwitch_StateChanged(object sender, Syncfusion.XForms.Buttons.SwitchStateChangedEventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //If calender turned on
                    if (calendarSwitch.IsOn == true)
                    {
                        //To request calender permission
                        Plugin.Permissions.Abstractions.PermissionStatus status = await CrossPermissions.Current.RequestPermissionAsync<CalendarPermission>();
                        //To check calender permission
                        Plugin.Permissions.Abstractions.PermissionStatus Checkstatus = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                        //If calender permission is granted
                        if (Checkstatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            //For android
                            if (Device.RuntimePlatform == Device.Android)
                            {
                                //To check location permission
                                Plugin.Permissions.Abstractions.PermissionStatus CheckLocation = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
                                //If location permission is granted
                                if (CheckLocation == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                                {
                                    //Add event to android calender
                                    DependencyService.Get<ICalenderDroid>().RID();
                                    DependencyService.Get<ICalenderDroid>().AddEventInAndroid(SDT);
                                }
                            }
                            //For iOS
                            else if (Device.RuntimePlatform == Device.iOS)
                            {
                                //Add event to iOS calender
                                DependencyService.Get<ICalenderDroid>().AddEventIniOS(SDT);
                            }
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "EditWatchParty.Xaml";
                addlogFile.ExceptionEventName = "calendarSwitch_StateChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        private void SfBusyIndicator()
        {
            busyIndicator.IsBusy = true;
            busyIndicator.AnimationType = AnimationTypes.Cupertino;
            busyIndicator.ViewBoxHeight = 50;
            busyIndicator.ViewBoxWidth = 50;
            busyIndicator.EnableAnimation = true;
            busyIndicator.Title = "Loading...";
            busyIndicator.TextSize = 12;
            busyIndicator.FontFamily = "Cabin";
            busyIndicator.TextColor = Color.FromHex("#9900CC");
            this.Content = busyIndicator;
        }

        private void Date_Tapped(object sender, EventArgs e)
        {
            date.IsOpen = !date.IsOpen;
        }

        async void date_OkButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            try
            {
                //To get selected date from datepicker
                var DatePickerValue = date.SelectedItem as ObservableCollection<object>; ;
                string DDate = DatePickerValue[0].ToString() + "/" + DatePickerValue[1].ToString() + "/" + DatePickerValue[2].ToString() + " " + "12:10:15 PM";
                //To convert string date into Datetime 
                CultureInfo culture = new CultureInfo("en-US");
                DateTime ConvertedDate = Convert.ToDateTime(DDate, culture);
                SHDate = ConvertedDate;
                SHTime = DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString();
                lblToday.Text = ConvertedDate.DayOfWeek + ", " + DatePickerValue[1].ToString() + " " + DatePickerValue[2].ToString() + ", " + DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString() + "  >";
                //Clear list before adding ScheduleDateTime
                SDT.Clear();
                //To add ScheduleDateTime and Title data to list
                SDT.Add(new ScheduleDateTime { Year = ConvertedDate.Year, Month = ConvertedDate.Month, Date = ConvertedDate.Day, Hour = Convert.ToInt32(DatePickerValue[3].ToString()), Minute = Convert.ToInt32(DatePickerValue[4].ToString()), Title = "Serena Watch Party" });
                #region Frame Date Time 
                var shDate1 = SHDate.Date.ToString("MMMM dd");
                datelbl.Text = shDate1.Substring(shDate1.Length - 2, 2) + " " + shDate1.Substring(0, 3);
                #endregion
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "EditWatchParty.Xaml";
                addlogFile.ExceptionEventName = "date_OkButtonClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
    }
}