﻿using Plugin.Toast;
using Serena.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThanksPopupScreen : ContentPage
    {
        public ThanksPopupScreen()
        {
            InitializeComponent();
            popupLoadingView.IsVisible = true;
        }

        async void Closebtn_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                popupLoadingView.IsVisible = false;
                await Navigation.PushAsync(new DoneWithClass());
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        async void Share_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                popupLoadingView.IsVisible = false;
                await Navigation.PushAsync(new DoneWithClass());
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
    }
}