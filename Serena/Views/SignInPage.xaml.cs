﻿using Serena.DBService.SocialMediaIntegration;
using Serena.ViewModel.SocialAndroid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignInPage : ContentPage
    {

        IOAuth2Service oAuth2Service;
        public SignInPage()
        {
            InitializeComponent();
            this.BindingContext = new SocialLoginPageViewModel(oAuth2Service);
            createaccLbl.Text = "Or tap to create a " + "SERENA" + " account";
        }

        async void SignUpClicked(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new SignUpPage());
            await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());
        }

        //protected override bool OnBackButtonPressed()
        //{
        //    base.OnBackButtonPressed();
        //    return true;
        //}
    }
}