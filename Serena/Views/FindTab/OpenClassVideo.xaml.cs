﻿using Xam.Forms.VideoPlayer;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Threading.Tasks;
using System.Linq;
using Xamarin.Essentials;
using Plugin.Toast;

namespace Serena.Views.FindTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenClassVideo : ContentPage
    {
        string sessionEmailId, reviewGuid, classGuid;
        string profileName = "";
        public OpenClassVideo(string guid)
        {
            try
            {
                InitializeComponent();
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    reviewGuid = classGuid = guid;
                    var findModel = App.DBIni.UserProfileDataDB.GetTopFindClassByGuid(guid);
                    if (findModel != null)
                    {
                        videoPlayer.IsVisible = true;
                        videoPlayer.AutoPlay = false;
                        var uriVideoSource = new UriVideoSource()
                        {
                            Uri = findModel.MediaUrl
                        };
                        videoPlayer.Source = uriVideoSource;
                        sessionEmailId = Settings.GeneralSettings;

                        lblAbout.Text = findModel.About;
                        var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(findModel.UserEmailId);
                        if (UserData != null)
                            lblUName.Text = !string.IsNullOrEmpty(UserData.USERNAME) ? UserData.USERNAME : UserData.FULLNAME;

                        var UserDataforSMS = App.DBIni.SignUpDB.GetSpecificUserByEmail(sessionEmailId);
                        if (UserDataforSMS != null)
                            profileName = !string.IsNullOrEmpty(UserDataforSMS.USERNAME) ? UserDataforSMS.USERNAME : UserDataforSMS.FULLNAME;

                        lblTags.Text = findModel.Tags;
                        lblTitle.Text = findModel.Title;
                        lblPlaylist.Text = findModel.PlayList;
                        lblCategory.Text = findModel.Category.ToUpper();
                        lblLevel.Text = findModel.Mentions;//level of difficulties
                        if (lblLevel.Text.ToLower() == "easy")
                        {
                            frmLevel.BackgroundColor = Color.FromHex("#3A9938");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        if (lblLevel.Text.ToLower() == "intermediate")
                        {
                            frmLevel.BackgroundColor = Color.FromHex("#262626");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        if (lblLevel.Text.ToLower() == "hard")
                        {
                            frmLevel.BackgroundColor = Color.FromHex("#E14545");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        lblCategories.Text = findModel.Category.ToUpper();
                        string repStr = findModel.Duration.Replace("Time: ", "");
                        string secs = repStr.Remove(0, 3);
                        string min = repStr.Substring(0, 2);

                        if (min != "00")
                            lblDuration.Text = min + " " + "MINS";

                        if (min == "01" || min == "1")
                            lblDuration.Text = min + " " + "MIN";

                        if (secs != "00")
                            lblDuration.Text = secs + " " + "SECS";
                        else
                            lblDuration.Text = min + " " + "MINS";

                        if (secs == "01" || secs == "1")
                            lblDuration.Text = secs + " " + "SEC";

                    }

                    #region review
                    var current1 = Connectivity.NetworkAccess;
                    if (current1 == NetworkAccess.Internet)
                    {
                        var serenaDataSync = new SerenaDataSync();
                        Task.Run(() => serenaDataSync.GetSerenaReviews(guid)).Wait();

                        var guidReviews = App.DBIni.ReviewsDB.GetAllReviewsByGuid(guid);
                        if (guidReviews != null && guidReviews.Count() > 0)
                            reviewList.ItemsSource = guidReviews;
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                    if (!string.IsNullOrEmpty(sessionEmailId))
                        videoPlayer.IsEnabled = true;
                    else
                        videoPlayer.IsEnabled = false;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "OpenClassVideo.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
            #endregion
        }

        private async void btnWatchParty_Clicked(object sender, System.EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (!string.IsNullOrEmpty(sessionEmailId))
                {
                    videoPlayer.Stop();
                    await Navigation.PushAsync(new SettingUpRoom(classGuid, lblCategories.Text, profileName));
                }
                else
                {
                    videoPlayer.AutoPlay = false;
                    videoPlayer.Stop();
                    bool value = await DisplayAlert("", "You can only schedule watch party as a member", "Login", "Cancel");
                    if (value == true)
                        await Navigation.PushModalAsync(new NavigationPage(new SignInPage()));

                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);


        }

        private async void btnInviteOthers_Clicked(object sender, EventArgs e)
        {
            // await SendSms(profileName.ToUpper() + " invited you to join a " + lblCategories.Text + " class.Join Serena now to participate: www.SerenaTest.com", "");
        }



        async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (!string.IsNullOrEmpty(sessionEmailId))
                {

                }
                else
                {
                    videoPlayer.AutoPlay = false;
                    videoPlayer.Stop();
                    bool value = await DisplayAlert("", "You can only see the class videos as a member", "Login", "Cancel");
                    if (value == true)
                        await Navigation.PushModalAsync(new NavigationPage(new SignInPage()));

                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }

        async void ReviewBtnClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (!string.IsNullOrEmpty(sessionEmailId))
                {
                    videoPlayer.Stop();
                    await Navigation.PushAsync(new WriteReview(reviewGuid));
                }
                else
                {
                    videoPlayer.AutoPlay = false;
                    videoPlayer.Stop();
                    bool value = await DisplayAlert("", "You can only add the reviews as a member", "Login", "Cancel");
                    if (value == true)
                        await Navigation.PushModalAsync(new NavigationPage(new SignInPage()));

                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }
        public async Task SendSms(string messageText, string recipient)
        {
            try
            {
                var message = new SmsMessage(messageText, new[] { recipient });
                await Sms.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Sms is not supported on this device.
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
    }
}