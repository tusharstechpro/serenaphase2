﻿using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Toast;
using Syncfusion.XForms.Buttons;
using Syncfusion.XForms.Graphics;
using Xamarin.Essentials;
using Serena.Helpers;

namespace Serena.Views.FindTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingUpRoom : ContentPage
    {
        List<GroupMembers> GM = new List<GroupMembers>();
        string classGuid;
        string categories, uName;
        public SettingUpRoom(string videoGuid, string category, string userName)
        {
            InitializeComponent();
            try
            {
                categories = category;
                uName = userName;
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    classGuid = videoGuid;

                    var followingUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(1);//followingUsers List
                    var followingCount = followingUsers.Count().ToString();
                    if (followingCount != "0")
                    {
                        var addUserProfileFollowingList = new List<UserProfileData>();
                        foreach (var user in followingUsers)
                        {
                            var followingUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                            UserProfileData addFollowingUserProfile = new UserProfileData();
                            addFollowingUserProfile.UserEmailId = followingUsersdata.UserEmailId;
                            addFollowingUserProfile.UserName = followingUsersdata.UserName;
                            addFollowingUserProfile.UserProfileImage = followingUsersdata.UserProfileImage;
                            addUserProfileFollowingList.Add(addFollowingUserProfile);
                        }
                        SuggestionsList.ItemsSource = addUserProfileFollowingList;
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SettingUpRoom.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //ask to join button click event
        void joinBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var sfButton = sender as SfButton;
                string RequestUserEmail = sfButton.CommandParameter.ToString();
                if (!string.IsNullOrEmpty(RequestUserEmail))
                {
                    var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(RequestUserEmail);
                    GM.Add(new GroupMembers { SuggestedUserEmailId = RequestUserEmail, UserName = userProfileData.UserName, UserProfileImage = userProfileData.UserProfileImage });
                    AddedBtnUI(sfButton);
                }

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SettingUpRoom.Xaml";
                addlogFile.ExceptionEventName = "joinBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static void AddedBtnUI(SfButton sfButton)
        {
            sfButton.IsEnabled = false;
            sfButton.TextColor = Color.FromHex("#262626");
            sfButton.BackgroundColor = Color.FromHex("#F4F4F4");
            sfButton.Text = "ADDED";
            sfButton.FontFamily = "Cabin";
            sfButton.FontSize = 12;
            sfButton.WidthRequest = 101;
            SfLinearGradientBrush linearGradientBrush = new SfLinearGradientBrush();
            linearGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                    new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                    new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
            sfButton.BackgroundGradient = linearGradientBrush;
        }

        //Next button click event
        private async void NextBtnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new ScheduleWatchParty(GM, classGuid, categories, uName));
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SettingUpRoom.Xaml";
                addlogFile.ExceptionEventName = "NextBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void ClearBtnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new Explore());
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SettingUpRoom.Xaml";
                addlogFile.ExceptionEventName = "ClearBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //Search present above followers list
        string FollowsearchBar = null;
        private void OnSearchTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                FollowsearchBar = txtSearch.Text;
                if (SuggestionsList.DataSource != null)
                {
                    this.SuggestionsList.DataSource.Filter = SearchUserName;
                    this.SuggestionsList.DataSource.RefreshFilter();
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SettingUpRoom.Xaml";
                addlogFile.ExceptionEventName = "OnSearchTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        private bool SearchUserName(object obj)
        {
            if (FollowsearchBar == null || FollowsearchBar == null)
                return true;

            var username = obj as UserProfileData;
            if (username.UserName.ToLower().Contains(FollowsearchBar.ToLower())
                 || username.UserName.ToLower().Contains(FollowsearchBar.ToLower()))
            {
                //FollowersListMessageLbl.IsVisible = false;
                return true;
            }
            else
            {
                //FollowersListMessageLbl.IsVisible = true;
                return false;
            }

        }
    }
}