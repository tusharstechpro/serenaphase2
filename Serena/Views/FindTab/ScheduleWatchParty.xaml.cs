﻿using Serena.Interface;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Permissions;
using Plugin.Toast;
using Serena.Helpers;
using System.Collections.ObjectModel;
using System.Globalization;
using Xamarin.Essentials;
using Syncfusion.SfBusyIndicator.XForms;
using System.ComponentModel;

namespace Serena.Views.FindTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScheduleWatchParty : ContentPage
    {
        List<GroupMembers> GM = new List<GroupMembers>();
        string videoGuid, sessionEmailId;
        List<ScheduleDateTime> SDT = new List<ScheduleDateTime>();
        string categories, uName;
        INotificationManager notificationManager;
        ICalenderDroid calendermanager;

        #region set default date to Datetime picker
        ObservableCollection<object> todaycollection = new ObservableCollection<object>();

        private ObservableCollection<object> _startdate;

        public ObservableCollection<object> StartDate
        {
            get { return _startdate; }
            set { _startdate = value; RaisePropertyChanged("StartDate"); }
        }

        void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        public ScheduleWatchParty(List<GroupMembers> groupMembers, string classGuid, string category, string userName)
        {
            InitializeComponent();
            categories = category;
            uName = userName;
            FinishInvitePeopleBtn.Text = "FINISH & INVITE PEOPLE";
            LoadInitialData(groupMembers, classGuid);
        }

        private void LoadInitialData(List<GroupMembers> groupMembers, string classGuid)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To get sessionEmailId value
                    sessionEmailId = Settings.GeneralSettings;
                    //To get Class GuId value from previous page
                    videoGuid = classGuid;
                    #region Display Group Members
                    //To get members data from previous page
                    GM = groupMembers;
                    //If GM is not null then set value to listview 
                    if (GM != null)
                    {
                        GroupMemberList.ItemsSource = GM;
                    }
                    #endregion

                    #region display dates on screen
                    //To get month name from number of month
                    System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                    //To display default datetime
                    lblToday.Text = "Today," + " " + mfi.GetMonthName(DateTime.Now.Month).ToString() + " " + DateTime.Now.Day.ToString() + ", " + DateTime.Now.ToString("HH:mm");
                    //To display time value only
                    lblTime.Text = DateTime.Now.ToString("HH:mm");
                    //set datetime value to use while creating watch party as a scheduleDate
                    lblSDate.Text = DateTime.Now.ToString();
                    //To display date value only
                    lblDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    #endregion

                    #region set current time format
                    //var format = DateTime.Now.ToString("h:mm tt");
                    //string timeFormat = format.Substring(format.Length - 2);
                    //if (timeFormat == "AM")
                    //{
                    //    TimeSwitch.SelectedSegment = 0;
                    //}
                    //else if (timeFormat == "PM")
                    //{
                    //    TimeSwitch.SelectedSegment = 1;
                    //}
                    #endregion

                    #region Set default date to datetime picker
                    //Get current time with format
                    var getTime = DateTime.Now.ToString("hh:mm tt");
                    //Remove ":" from time string
                    var subgetTime = getTime.Replace(":", "");
                    //Get format from time string
                    var TFormat = subgetTime.Remove(0, 5);
                    //Add current year to datetime Picker
                    todaycollection.Add(DateTime.Now.Year.ToString());
                    //Add current month to datetime Picker
                    todaycollection.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month).Substring(0, 3));
                    //Add current date to datetime Picker
                    if (DateTime.Now.Day < 10)
                        todaycollection.Add("0" + DateTime.Now.Day);
                    else
                        todaycollection.Add(DateTime.Now.Day.ToString());
                    //Add current Hour to datetime Picker
                    todaycollection.Add(DateTime.Now.Hour);
                    //Add current Minute to datetime Picker
                    todaycollection.Add(DateTime.Now.Minute);
                    //Add current Time format to datetime Picker
                    todaycollection.Add(TFormat);
                    this.StartDate = todaycollection;
                    date.SelectedItem = StartDate;
                    #endregion

                    //To add ScheduleDateTime and Title data to list
                    SDT.Add(new ScheduleDateTime { Year = DateTime.Now.Year, Month = DateTime.Now.Month, Date = DateTime.Now.Day, Hour = DateTime.Now.Hour, Minute = DateTime.Now.Minute, Title = "Serena Watch Party" });
                    //Notification service init
                    notificationManager = DependencyService.Get<INotificationManager>();
                    calendermanager = DependencyService.Get<ICalenderDroid>();
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Time format switch change event
        //void TimeFormatSwitchChanged(object sender, System.EventArgs e)
        //{
        //    try
        //    {
        //        if (TimeSwitch.SelectedSegment == 1) //AM
        //        {
        //            TimeFormatLbl.Text = "PM";
        //        }
        //        else if (TimeSwitch.SelectedSegment == 0) //PM
        //        {
        //            TimeFormatLbl.Text = "AM";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var addlogFile = new ExceptionLog();
        //        addlogFile.ExceptionDetails = ex.Message;
        //        addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
        //        addlogFile.ExceptionEventName = "TimeFormatSwitchChanged";

        //        App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
        //    }
        //}

        //Finish button click event
        async void FinishBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var findModel = App.DBIni.UserProfileDataDB.GetTopFindClassByGuid(videoGuid);
                    if (findModel != null)
                    {
                        SfBusyIndicator busyIndicator = new SfBusyIndicator()
                        {
                            AnimationType = AnimationTypes.Cupertino,
                            ViewBoxHeight = 50,
                            ViewBoxWidth = 50,
                            EnableAnimation = true,
                            Title = "Loading...",
                            TextSize = 12,
                            FontFamily = "Cabin",
                            TextColor = Color.FromHex("#9900CC")
                        };
                        this.Content = busyIndicator;

                        //To get selected date from datepicker
                        var DatePickerValue = date.SelectedItem as ObservableCollection<object>; ;
                        //To get selected time format
                        //string format = TimeSwitch.SelectedSegment == 0 ? "AM" : "PM";
                        string format = DatePickerValue[5].ToString();

                        #region  add event in calender
                        //If calender turned on
                        if (calendarSwitch.IsOn == true)
                        {
                            //To request calender permission
                            Plugin.Permissions.Abstractions.PermissionStatus status = await CrossPermissions.Current.RequestPermissionAsync<CalendarPermission>();
                            //To check calender permission
                            Plugin.Permissions.Abstractions.PermissionStatus Checkstatus = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                            //If calender permission is granted
                            if (Checkstatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                            {
                                //For android
                                if (Device.RuntimePlatform == Device.Android)
                                {
                                    //To check location permission
                                    Plugin.Permissions.Abstractions.PermissionStatus CheckLocation = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
                                    //If location permission is granted
                                    if (CheckLocation == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                                    {
                                        //Add event to android calender
                                        calendermanager.RID();
                                        calendermanager.AddEventInAndroid(SDT);
                                    }
                                }
                                //For iOS
                                else if (Device.RuntimePlatform == Device.iOS)
                                {
                                    //Add event to iOS calender
                                    DependencyService.Get<ICalenderDroid>().AddEventIniOS(SDT);
                                }
                            }
                        }
                        #endregion

                        #region To generate notification via app
                        if (viaAppSwitch.IsOn == true)
                        {
                            string title = $"Join a " + findModel.Title + " class at " + lblTime.Text + format + " today!";
                            string message = $"Open up your app to see details about this upcoming class and accept the invitation.";
                            //create a datetime value
                            var dates = DatePickerValue[0].ToString() + "/" + DatePickerValue[1].ToString() + "/" + DatePickerValue[2].ToString() + " " + DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString() + ":" + "00" + " " + format;
                            //To convert string to datetime
                            CultureInfo culture = new CultureInfo("en-US");
                            //To convert string to datetime
                            DateTime notifyDate = Convert.ToDateTime(dates, culture);
                            //To get 5 minutes prior datetime
                            DateTime notifyTime = notifyDate.AddMinutes(-5);
                            //To pass notification details to SendNotification
                            notificationManager.SendNotification(title, message, notifyTime);
                        }
                        #endregion

                        #region watch party
                        //Create new GuId as per sessionEnailId
                        Guid guid = Guid.NewGuid();
                        string WatchPartyGuid = guid.ToString();
                        WatchPartyGuid = (guid + sessionEmailId);
                        //If GuId IsNullOrEmpty then set value "-"
                        if (string.IsNullOrEmpty(WatchPartyGuid))
                        {
                            WatchPartyGuid = "-";
                        }
                        //To add data to new WatchParty model
                        var watchParty = new WatchParty();
                        var watchParty_Out = new WatchParty_Out();
                        watchParty_Out.Guid = watchParty.Guid = WatchPartyGuid;
                        watchParty_Out.UserEmailId = watchParty.UserEmailId = Settings.GeneralSettings;
                        watchParty_Out.ClassGuid = watchParty.ClassGuid = videoGuid;
                        watchParty_Out.ScheduleDate = watchParty.ScheduleDate = Convert.ToDateTime(lblSDate.Text);
                        watchParty_Out.ScheduleTime = watchParty.ScheduleTime = lblTime.Text;
                        watchParty_Out.AMPM = watchParty.AMPM = format;
                        watchParty_Out.MarkToCalendar = watchParty.MarkToCalendar = calendarSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.RemindViaApp = watchParty.RemindViaApp = viaAppSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.RemindViaTxt = watchParty.RemindViaTxt = viaTextSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.WPApiStatus = "0";//Create
                        watchParty_Out.LastSyncDate = DateTime.Now;

                        //adding in sqllite
                        App.DBIni.WatchPartyDB.AddWatchParty(watchParty);

                        //adding in sqllite out table
                        App.DBIni.WatchPartyDB.AddWatchParty_Out(watchParty_Out);
                        #endregion

                        #region watch party tran 
                        StringBuilder stringJson = new StringBuilder();
                        foreach (var members in GM)
                        {
                            //To add watch party members data
                            var watchPartyTran = new WatchPartyTran();
                            var watchPartyTran_Out = new WatchPartyTran_Out();
                            watchPartyTran_Out.WatchPartyGuid = watchPartyTran.WatchPartyGuid = WatchPartyGuid;
                            watchPartyTran_Out.Members = watchPartyTran.Members = members.SuggestedUserEmailId;
                            watchPartyTran_Out.Accepted = watchPartyTran.Accepted = "0";
                            watchPartyTran_Out.LastSyncDate = DateTime.Now;

                            //adding in sqllite
                            App.DBIni.WatchPartyDB.AddWatchPartyTran(watchPartyTran);

                            //adding in sqllite out
                            App.DBIni.WatchPartyDB.AddWatchPartyTran_Out(watchPartyTran_Out);
                        }
                        #endregion

                        #region add data to upcoming_notification table

                        Upcoming_Notifications sn = new Upcoming_Notifications();
                        sn.UserEmailId = sessionEmailId;
                        sn.ClassGuid = videoGuid;
                        sn.MediaUrl = findModel.MediaUrl;
                        sn.CoverPhotoUrl = findModel.CoverPhotoUrl;
                        sn.Title = findModel.Title;
                        sn.About = findModel.About;
                        sn.Category = findModel.Category;
                        sn.Tags = findModel.Tags;
                        sn.Mentions = findModel.Mentions;
                        sn.Duration = findModel.Duration;
                        sn.WatchPartyGuid = WatchPartyGuid;
                        sn.ScheduledDate = Convert.ToDateTime(lblSDate.Text);
                        sn.ScheduledTime = lblTime.Text;
                        sn.AMPM = format;
                        sn.HostEmailID = sessionEmailId;
                        sn.Accepted = "0";
                        sn.AvgRating = findModel.AvgRating.ToString();
                        sn.TotalReviews = findModel.TotalReviews;
                        sn.Members = "";
                        var notifyData = App.DBIni.NotificationDB.AddUpcomingNotifications(sn);
                        #endregion
                    }
                    await Navigation.PushAsync(new Explore());
                    await (App.Current.MainPage as Xamarin.Forms.Shell).GoToAsync("//tabbar/tabRouteACTIVITY/shellRouteACTIVITY", true);

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "FinishBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //To cancel watch party creation and go back to explore classes
        async void ClearBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new Explore());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "ClearBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //To open datetime selector
        void onDateTapped(object sender, EventArgs e)
        {
            try
            {
                date.IsOpen = !date.IsOpen;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "onDateTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //To get datetime value on OK click
        private void OKDateButtonClicked(object sender, Syncfusion.SfPicker.XForms.SelectionChangedEventArgs e)
        {
            try
            {
                //To get selected date from datepicker
                var DatePickerValue = date.SelectedItem as ObservableCollection<object>; ;
                //To get collected date in the string format
                string DDate = DatePickerValue[0].ToString() + "/" + DatePickerValue[1].ToString() + "/" + DatePickerValue[2].ToString() + " " + "12:10:15 PM";
                //To convert string date into Datetime 
                CultureInfo culture = new CultureInfo("en-US");
                //To convert string date into Datetime 
                DateTime ConvertedDate = Convert.ToDateTime(DDate, culture);
                //To display datetime value on screen
                lblToday.Text = ConvertedDate.DayOfWeek + ", " + DatePickerValue[0].ToString() + " " + DatePickerValue[1].ToString() + ", " + DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString();
                //To display time value only
                lblTime.Text = DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString();
                //To display date value only
                lblDate.Text = ConvertedDate.ToString("MM/dd/yyyy");
                //set datetime value to use while creating watch party as a scheduleDate
                lblSDate.Text = ConvertedDate.ToString();
                //To clear ScheduleDateTime before adding value
                SDT.Clear();
                //To add ScheduleDateTime and Title data to list
                SDT.Add(new ScheduleDateTime { Year = ConvertedDate.Year, Month = ConvertedDate.Month, Date = ConvertedDate.Day, Hour = Convert.ToInt32(DatePickerValue[3].ToString()), Minute = Convert.ToInt32(DatePickerValue[4].ToString()), Title = "Serena Watch Party" });

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "OKDateButtonClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Finish and Invite button click event
        async void FinishInvitePeopleBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var findModel = App.DBIni.UserProfileDataDB.GetTopFindClassByGuid(videoGuid);
                    if (findModel != null)
                    {

                        SfBusyIndicator busyIndicator = new SfBusyIndicator()
                        {
                            AnimationType = AnimationTypes.Cupertino,
                            ViewBoxHeight = 50,
                            ViewBoxWidth = 50,
                            EnableAnimation = true,
                            Title = "Loading...",
                            TextSize = 12,
                            FontFamily = "Cabin",
                            TextColor = Color.FromHex("#9900CC")
                        };
                        this.Content = busyIndicator;

                        //To get selected date from datepicker
                        var DatePickerValue = date.SelectedItem as ObservableCollection<object>; ;
                        //To get selected time format
                        //string format = TimeSwitch.SelectedSegment == 0 ? "AM" : "PM";
                        string format = DatePickerValue[5].ToString();

                        #region  add event in calender
                        //If calender turned on
                        if (calendarSwitch.IsOn == true)
                        {
                            //To request calender permission
                            Plugin.Permissions.Abstractions.PermissionStatus status = await CrossPermissions.Current.RequestPermissionAsync<CalendarPermission>();
                            //To check calender permission
                            Plugin.Permissions.Abstractions.PermissionStatus Checkstatus = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                            //If calender permission is granted
                            if (Checkstatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                            {
                                //For android
                                if (Device.RuntimePlatform == Device.Android)
                                {
                                    //To check location permission
                                    Plugin.Permissions.Abstractions.PermissionStatus CheckLocation = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
                                    //If location permission is granted
                                    if (CheckLocation == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                                    {
                                        //Add event to android calender
                                        calendermanager.RID();
                                        calendermanager.AddEventInAndroid(SDT);
                                    }
                                }
                                //For iOS
                                else if (Device.RuntimePlatform == Device.iOS)
                                {
                                    //Add event to iOS calender
                                    DependencyService.Get<ICalenderDroid>().AddEventIniOS(SDT);
                                }
                            }
                        }
                        #endregion

                        #region To generate notification via app
                        if (viaAppSwitch.IsOn == true)
                        {
                            string title = $"Join a " + findModel.Title + " class at " + lblTime.Text + format + " today!";
                            string message = $"Open up your app to see details about this upcoming class and accept the invitation.";
                            //create a datetime value
                            var dates = DatePickerValue[0].ToString() + "/" + DatePickerValue[1].ToString() + "/" + DatePickerValue[2].ToString() + " " + DatePickerValue[3].ToString() + ":" + DatePickerValue[4].ToString() + ":" + "00" + " " + format;
                            //To convert string to datetime
                            CultureInfo culture = new CultureInfo("en-US");
                            //To convert string to datetime
                            DateTime notifyDate = Convert.ToDateTime(dates, culture);
                            //To get 5 minutes prior datetime
                            DateTime notifyTime = notifyDate.AddMinutes(-5);
                            //To pass notification details to SendNotification
                            notificationManager.SendNotification(title, message, notifyTime);
                        }
                        #endregion

                        #region watch party
                        //Create new GuId as per sessionEnailId
                        Guid guid = Guid.NewGuid();
                        string WatchPartyGuid = guid.ToString();
                        WatchPartyGuid = (guid + sessionEmailId);
                        //If GuId IsNullOrEmpty then set value "-"
                        if (string.IsNullOrEmpty(WatchPartyGuid))
                        {
                            WatchPartyGuid = "-";
                        }
                        //To add data to new WatchParty model
                        var watchParty = new WatchParty();
                        var watchParty_Out = new WatchParty_Out();
                        watchParty_Out.Guid = watchParty.Guid = WatchPartyGuid;
                        watchParty_Out.UserEmailId = watchParty.UserEmailId = Settings.GeneralSettings;
                        watchParty_Out.ClassGuid = watchParty.ClassGuid = videoGuid;
                        watchParty_Out.ScheduleDate = watchParty.ScheduleDate = Convert.ToDateTime(lblSDate.Text);
                        watchParty_Out.ScheduleTime = watchParty.ScheduleTime = lblTime.Text;
                        watchParty_Out.AMPM = watchParty.AMPM = format;
                        watchParty_Out.MarkToCalendar = watchParty.MarkToCalendar = calendarSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.RemindViaApp = watchParty.RemindViaApp = viaAppSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.RemindViaTxt = watchParty.RemindViaTxt = viaTextSwitch.IsOn == true ? "ON" : "OFF";
                        watchParty_Out.LastSyncDate = DateTime.Now;
                        watchParty_Out.WPApiStatus = "0";//Create
                        //adding in sqllite normal table
                        App.DBIni.WatchPartyDB.AddWatchParty(watchParty);

                        //adding in sqllite out table
                        App.DBIni.WatchPartyDB.AddWatchParty_Out(watchParty_Out);
                        #endregion

                        #region watch party tran 

                        foreach (var members in GM)
                        {
                            //To add watch party members data
                            var watchPartyTran = new WatchPartyTran();
                            var watchPartyTran_Out = new WatchPartyTran_Out();
                            watchPartyTran_Out.WatchPartyGuid = watchPartyTran.WatchPartyGuid = WatchPartyGuid;
                            watchPartyTran_Out.Members = watchPartyTran.Members = members.SuggestedUserEmailId;
                            watchPartyTran_Out.Accepted = watchPartyTran.Accepted = "0";
                            watchPartyTran_Out.LastSyncDate = DateTime.Now;

                            //adding in sqllite
                            App.DBIni.WatchPartyDB.AddWatchPartyTran(watchPartyTran);

                            //adding in sqllite out
                            App.DBIni.WatchPartyDB.AddWatchPartyTran_Out(watchPartyTran_Out);
                        }
                        #endregion

                        #region add data to upcoming_notification table
                        if (findModel != null)
                        {
                            Upcoming_Notifications sn = new Upcoming_Notifications();
                            sn.UserEmailId = sessionEmailId;
                            sn.ClassGuid = videoGuid;
                            sn.MediaUrl = findModel.MediaUrl;
                            sn.CoverPhotoUrl = findModel.CoverPhotoUrl;
                            sn.Title = findModel.Title;
                            sn.About = findModel.About;
                            sn.Category = findModel.Category;
                            sn.Tags = findModel.Tags;
                            sn.Mentions = findModel.Mentions;
                            sn.Duration = findModel.Duration;
                            sn.WatchPartyGuid = WatchPartyGuid;
                            sn.ScheduledDate = Convert.ToDateTime(lblSDate.Text);
                            sn.ScheduledTime = lblTime.Text;
                            sn.AMPM = format;
                            sn.HostEmailID = sessionEmailId;
                            sn.Accepted = "0";
                            sn.AvgRating = findModel.AvgRating.ToString();
                            sn.TotalReviews = findModel.TotalReviews;
                            sn.Members = "";
                            var notifyData = App.DBIni.NotificationDB.AddUpcomingNotifications(sn);
                        }
                        #endregion
                    }

                    #region send sms

                    await SendSms(uName.ToUpper() + " invited you to join a " + categories + " class.Open your serena app or join now to participate: www.link.com/gotoapp.", "");

                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "FinishBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Send SMS 
        public async Task SendSms(string messageText, string recipient)
        {
            try
            {
                var message = new SmsMessage(messageText, new[] { recipient });
                await Sms.ComposeAsync(message);
                await Navigation.PushAsync(new Explore());
                await (App.Current.MainPage as Xamarin.Forms.Shell).GoToAsync("//tabbar/tabRouteACTIVITY/shellRouteACTIVITY", true);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Sms is not supported on this device.
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

        //To add events on calender
        private async void calendarSwitch_StateChanged(object sender, Syncfusion.XForms.Buttons.SwitchStateChangedEventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //If calender turned on
                    if (calendarSwitch.IsOn == true)
                    {
                        //To request calender permission
                        Plugin.Permissions.Abstractions.PermissionStatus status = await CrossPermissions.Current.RequestPermissionAsync<CalendarPermission>();
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ScheduleWatchParty.Xaml";
                addlogFile.ExceptionEventName = "calendarSwitch_StateChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
    }
}