﻿using Plugin.Toast;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogInUsers : ContentPage
    {
        CryptographyASE Crypto = new CryptographyASE();
        public LogInUsers()
        {
            InitializeComponent();
        }

        //If User is new then navigate to signUpPage
        async void NewUserLblClicked(object sender, EventArgs e)
        {
            try
            {
                //await Navigation.PushAsync(new SignUpPage());
                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LoginUser.Xaml";
                addlogFile.ExceptionEventName = "NewUserLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        //Sign In Button Click event
        async void SignInClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                // Connection to internet is available
                if (current == NetworkAccess.Internet)
                {
                    //Empty field validations
                    if (string.IsNullOrEmpty(Email.Text) || string.IsNullOrEmpty(Password.Text))
                    {
                        if (string.IsNullOrEmpty(Email.Text))
                        {
                            input1.HasError = true;
                        }
                        if (string.IsNullOrEmpty(Password.Text))
                        {
                            input2.HasError = true;

                        }
                    }
                    else
                    {
                        //Get specific userdata by emailId from Serena_Users table  SQLite
                        var usersData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Email.Text);
                        //var postdata = signupDB.GetUsers();
                        if (usersData != null)
                        {

                            //Compare decrypted password and entered password 
                            string passToCompare = Crypto.Decrypt(usersData.PASSWORD);
                            if (string.IsNullOrEmpty(passToCompare))
                            {
                                input2.HasError = true;
                                input2.ErrorText = "Password Incorrect";
                            }

                            if (usersData.EMAIL == null)
                            {
                                input1.HasError = true;
                                input1.ErrorText = "There is no user registered with this Email";
                            }
                            else if (Password.Text != passToCompare)
                            {
                                input2.HasError = true;
                                input2.ErrorText = "Password Incorrect";
                            }
                            else if (Email.Text == usersData.EMAIL && passToCompare == Password.Text)
                            {
                                //To apply loading... as a busy indicator
                                SfBusyIndicator busyIndicator = new SfBusyIndicator()
                                {
                                    AnimationType = AnimationTypes.Cupertino,
                                    ViewBoxHeight = 50,
                                    ViewBoxWidth = 50,
                                    EnableAnimation = true,
                                    Title = "Loading...",
                                    TextSize = 12,
                                    FontFamily = "Cabin",
                                    TextColor = Color.FromHex("#9900CC")
                                };
                                this.Content = busyIndicator;

                                //add userEmailId to session
                                Settings.GeneralSettings = "";
                                Settings.GeneralSettings = Email.Text;

                                //navigate to ProfileMasterPage
                                ///await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new SerenaTabbedPage()));//crashing
                                //await Navigation.PushAsync(new NavigationPage(new SerenaTabbedPage()));//crashing
                                //await Navigation.PushAsync(new  SerenaTabbedPage());//crashing
                                //await Shell.Current.GoToAsync(nameof(SerenaTabbedPage));//crashing

                                //await (App.Current.MainPage as Xamarin.Forms.Shell).GoToAsync("//tabbar/tabRouteFind/shellRouteFind", true);//navigating but crashing when navigating to first tab page

                                //await Shell.Current.GoToAsync("//tabRouteFind/shellRouteFind");//crashing
                                //await Shell.Current.GoToAsync("tabRouteFind");//loading...
                                //MainPage = new SerenaTabbedPage();
                                // await Navigation.PushAsync(new Explore());//exception pushasync
                                //await Navigation.PushAsync(new NavigationPage(new Explore()));//exception pushasync


                                await (App.Current.MainPage as Xamarin.Forms.Shell).GoToAsync("//tabbar/tabRouteFind/shellRouteFind", true);//popout all previous pages and display shellpage (issue is displaying hamburger menu in first tab page view)
                            }
                        }
                        else
                        {
                            input1.HasError = true;
                            input1.ErrorText = "Invalid User";
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LoginUser.Xaml";
                addlogFile.ExceptionEventName = "SignInClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //EmailTextChange event
        private void EmailTextChanged(object sender, EventArgs e)
        {
            input1.HasError = false;
            input1.ErrorText = "";
        }
        //PasswordTextChange event
        private void PasswordTextChanged(object sender, EventArgs e)
        {
            input2.HasError = false;
            input2.ErrorText = "";
        }
    }
}