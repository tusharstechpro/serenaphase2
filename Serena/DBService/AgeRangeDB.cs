﻿using Serena.Interface;
using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class AgeRangeDB
    {
        //private SQLiteConnection _SQLiteConnection;

        private SQLiteConnection _SQLiteConnection;

        private static object collisionLock = new object(); //amd 2nd feb

        public AgeRangeDB()
        {
            // Device Platform Switch Removed AMD 2Feb
            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<AgeRange>();
            _SQLiteConnection.CreateTable<LastSync>();
        }

        #region AgeRange services
        public IEnumerable<AgeRange> GetAgeData()
        {
            return (from u in _SQLiteConnection.Table<AgeRange>()
                    select u).ToList();

        }

        public void DeleteAge(int id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Delete<AgeRange>(id);
            }
        }

        public string AddAge(AgeRange age)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<AgeRange>();
                var d1 = data.Where(x => x.AgeRangeName == age.AgeRangeName).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(age);
                    return "Sucessfully Added";
                }
                else
                {
                    age.Id = d1.Id;
                    updateAge(age);
                    return "Age Already Exist";
                }
            }
        }
        public bool updateAge(AgeRange AgeData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(AgeData);
                return true;
            }
        }
        #endregion

        #region Last_Sync services
        public IEnumerable<LastSync> GetLastSyncData()
        {
            return (from u in _SQLiteConnection.Table<LastSync>()
                    select u).ToList();

        }
        //Get last sync data by table_name
        public LastSync GetSyncDataByTableName(string Table_Name)
        {
            return _SQLiteConnection.Table<LastSync>().FirstOrDefault(t => t.TableName == Table_Name);

        }
        public string AddLastSync(LastSync ls)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<LastSync>();
                var d1 = data.Where(x => x.TableName == ls.TableName).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(ls);
                    return "Sucessfully Added";
                }
                else
                {
                    ls.Id = d1.Id;
                    updateLastSync(ls);
                    return "Already Exist";
                }
            }
        }
        public bool updateLastSync(LastSync ls)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Update(ls);
                return true;
            }
        }
        #endregion
    }
}
