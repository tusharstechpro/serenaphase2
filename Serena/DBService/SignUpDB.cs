﻿using Serena.Interface;
using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class SignUpDB
    {
        private SQLiteConnection _SQLiteConnection;

        private static object collisionLock = new object(); //amd 2nd feb
        public SignUpDB()
        {

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _ = _SQLiteConnection.CreateTable<SignUpModel>();
            _ = _SQLiteConnection.CreateTable<SignUpModel_Out>();
        }

        #region SignUp
        public IEnumerable<SignUpModel> GetUsers()
        {


            return (from u in _SQLiteConnection.Table<SignUpModel>()
                    select u).ToList();

        }
        public SignUpModel GetSpecificUserByUserName(string UserName)
        {


            return _SQLiteConnection.Table<SignUpModel>().FirstOrDefault(t => t.USERNAME == UserName);

        }

        public SignUpModel GetSpecificUserByEmail(string Id)
        {


            return _SQLiteConnection.Table<SignUpModel>().FirstOrDefault(t => t.EMAIL == Id);


        }
        public void DeleteUser(string id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _ = _SQLiteConnection.Delete<SignUpModel>(id);
            }
        }

        public string AddUser(SignUpModel registration)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<SignUpModel>();
                var d1 = data.Where(x => x.EMAIL == registration.EMAIL).FirstOrDefault();
                if (d1 == null)
                {
                    _ = _SQLiteConnection.Insert(registration);
                    return "Sucessfully Added";
                }
                else
                    return "Already Mail id Exist";
            }
        }
        public bool updateUser(string emailid, string pwd)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                var data = _SQLiteConnection.Table<SignUpModel>();
                var d1 = (from values in data
                          where values.EMAIL == emailid
                          select values).Single();
                if (true)
                {
                    d1.PASSWORD = pwd;
                    _ = _SQLiteConnection.Update(d1);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region SignUpOut
        public string AddUser_out(SignUpModel_Out registration)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _ = _SQLiteConnection.Insert(registration);
                return "Sucessfully Added";
            }
        }

        public IEnumerable<SignUpModel_Out> GetAllUser_Out(DateTime out_sync_date, string status)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                return (from u in _SQLiteConnection.Table<SignUpModel_Out>()
                        select u).Where(x => x.LastSyncDate > out_sync_date && x.Status == status).ToList();//pending
            }
        }

        public SignUpModel_Out GetOutUserByEmail(string Id)
        {
            return _SQLiteConnection.Table<SignUpModel_Out>().FirstOrDefault(t => t.EMAIL == Id);
        }

        #endregion

    }
}
