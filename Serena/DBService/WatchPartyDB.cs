﻿using Serena.Interface;
using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace Serena.DBService
{
    public class WatchPartyDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb

        public WatchPartyDB()
        {
            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<WatchParty>();
            _SQLiteConnection.CreateTable<WatchPartyTran>();
            _SQLiteConnection.CreateTable<WatchParty_Out>();
            _SQLiteConnection.CreateTable<WatchPartyTran_Out>();


        }


        #region WatchParty
        public string AddWatchParty(WatchParty watchParty)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<WatchParty>();
                var d1 = data.Where(x => x.Guid == watchParty.Guid).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(watchParty);
                    return "Sucessfully Added";
                }
                else
                {
                    watchParty.Id = d1.Id;
                    updateWatchParty(watchParty);
                    return "Already  Exist";

                }
            }
        }

        public bool updateWatchParty(WatchParty watchPartydata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(watchPartydata);
                return true;
            }
        }

        public string DeleteWPByGuId(string WPGuid)
        {
            var data = _SQLiteConnection.Table<WatchParty>();
            var d1 = data.Where(x => x.Guid == WPGuid).FirstOrDefault();
            if (d1 != null)
            {
                _SQLiteConnection.Delete(d1);
                return "Deleted";
            }
            else
            {
                return "Record not found!";
            }
        }
        #endregion

        #region WatchPartyTran
        public void DeleteAllWatchPartyTran()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.DeleteAll<WatchPartyTran>();
            }
        }
        public string AddWatchPartyTran(WatchPartyTran watchPartyTran)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Insert(watchPartyTran);
                return "Sucessfully Added";
            }
        }

        public IEnumerable<WatchPartyTran> GetWPMembersByWPGuId(string WPGuid)
        {
            return (from usr in _SQLiteConnection.Table<WatchPartyTran>()
                    select usr).Where(x => x.WatchPartyGuid == WPGuid)
                .OrderBy(x => x.Id).ToList();
        }
        public string DeleteWPTByGuId(string WPGuid)
        {
            var data = _SQLiteConnection.Table<WatchPartyTran>();
            var d1 = data.Where(x => x.WatchPartyGuid == WPGuid).FirstOrDefault();
            if (d1 != null)
            {
                _SQLiteConnection.Delete(d1);
                return "Deleted";
            }
            else
            {
                return "Record not found!";
            }
        }

        #endregion


        #region Watchparty_Out - Master table


        public string AddWatchParty_Out(WatchParty_Out watchParty)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Insert(watchParty);
                return "Sucessfully Added";
            }
        }

        //to get watchparty out data between time interval
        public IEnumerable<WatchParty_Out> GetAllWatchPartyOut(DateTime out_sync_date, string status)
        {
            return (from u in _SQLiteConnection.Table<WatchParty_Out>()
                    select u).Where(x => x.LastSyncDate > out_sync_date && x.WPApiStatus == status).ToList();
        }

        public WatchParty_Out GetWPByWPGuId(string WPGuid)
        {
            return _SQLiteConnection.Table<WatchParty_Out>().FirstOrDefault(t => t.Guid == WPGuid);
        }

        #endregion

        #region Watchpartytran_Out - Transaction table

        public string AddWatchPartyTran_Out(WatchPartyTran_Out watchPartyTran)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Insert(watchPartyTran);
                return "Sucessfully Added";
            }
        }

        public IEnumerable<WatchPartyTran_Out> GetAllWatchPartyTranOutByStatus(DateTime out_sync_date, string status)
        {
            return (from u in _SQLiteConnection.Table<WatchPartyTran_Out>()
                    select u).Where(x => x.LastSyncDate > out_sync_date && x.Accepted == status).ToList();

        }
        #endregion
    }
}
