﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Serena.DBService
{
    public class SerenaDataSync
    {
        public static string FilePath;
        string sessionEmailId;

        public SerenaDataSync()
        {
            sessionEmailId = Settings.GeneralSettings;
        }

        #region Change user status

        public async Task AcceptRejectUserRequest(string Guid, string status)
        {
            try
            {
                var payload = new Dictionary<string, string>
                         {
                            {"Guid", Guid},
                             {"Status", status}
                            };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://q9xs4q70n9.execute-api.ap-southeast-2.amazonaws.com/serena/updateuptranrdsnew";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "InsertUserProfileTran";

            }
        }
        public async Task InsertUserProfileTran(string sessionEmailId, string RequestUserEmail, string Guidstr, string status)//0-sendrequest, 1-acceptrequest
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                            {"UserEmailId", sessionEmailId},
                            {"RequestTo", RequestUserEmail},
                            {"Status", status},
                            {"Guid", Guidstr},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://nwy2dc4yjc.execute-api.ap-southeast-2.amazonaws.com/serena/insertuptranrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "InsertUserProfileTran";

            }
        }
        #endregion

        #region reviews and top people api

        public async Task GetSerenaReviews(string revGuid)
        {
            try
            {
                App.DBIni.ReviewsDB.DeleteAllReviews();

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", revGuid}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://b42vumkv1k.execute-api.ap-southeast-2.amazonaws.com/serena/getpostreviewsrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                var sm = new SerenaReviews();

                                double avgrating = 0;
                                dynamic Rating = item.Rating;
                                if (Rating == "" || Rating == null)
                                    avgrating = 0;
                                else
                                    avgrating = Math.Round(Convert.ToDouble(Rating), 1);

                                DateTime DateAdded = item.DateAdded;

                                sm.Guid = revGuid;
                                sm.UserEmailId = item.UserEmailId;
                                sm.UserName = item.FullName;
                                sm.Review = item.Review;
                                sm.Rating = avgrating;
                                sm.DateAdded = DateAdded.ToString("MM/dd/yyyy");

                                App.DBIni.ReviewsDB.AddReview(sm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExplorePostsByHashTag";
            }
        }

        public async Task PostSerenaReviewsByGuid_Class(SerenaReviews_Out serenaReviews)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", serenaReviews.Guid},
                        {"UserEmailId", serenaReviews.UserEmailId},
                        {"Review", serenaReviews.Review},
                        {"Rating", Convert.ToString(serenaReviews.Rating)}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://e8u6fgnoci.execute-api.ap-southeast-2.amazonaws.com/serena/savepostreviewrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }
        public async Task SaveProClass(UploadMediaModel_Out uploadMedia)
        {
            try
            {

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", uploadMedia.Guid},
                        {"UserEmailId", uploadMedia.UserEmailId},
                        {"MediaUrl", uploadMedia.MediaUrl},
                        {"CoverPhotoUrl", uploadMedia.CoverPhotoUrl},
                        {"Title", uploadMedia.Title},
                        {"About", uploadMedia.About},
                        {"PlayList", uploadMedia.PlayList},
                        {"Category", uploadMedia.Category},
                        {"Tags", uploadMedia.Tags},
                        {"Mentions", uploadMedia.Mentions},
                        {"Duration", uploadMedia.Duration},

                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://w70k0qq0lh.execute-api.ap-southeast-2.amazonaws.com/serena/saveproclass";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveProClass";

            }
        }
        public async Task GetTopPeopleExplore()
        {
            try
            {
                //deleteing each record everytime
                // App.DBIni.TophashtagDB.DeleteAllTopPeople();
                var url = "https://0fk4gf2lpf.execute-api.ap-southeast-2.amazonaws.com/serena/gettoppeople";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {
                                    var topPeople = new People();


                                    dynamic item4 = item.AvgRating;
                                    double rating = 0;
                                    dynamic item11 = item.AvgRating;
                                    if (item11 == "" || item11 == null)
                                        rating = 0;
                                    else
                                        rating = Math.Round(Convert.ToDouble(item11), 1);


                                    topPeople.UserEmailId = item.UserEmailId;
                                    topPeople.UserName = item.UserName;
                                    topPeople.UserProfileImage = item.UserProfileImage;
                                    topPeople.AvgRating = rating;
                                    topPeople.TotalReviews = item.TotalReviews;
                                    topPeople.Topics = item.Topics;

                                    App.DBIni.TophashtagDB.AddTopPeople(topPeople);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        public async Task GetProclassForAll()
        {
            try
            {
                //deleteing all classes from session user
                //App.DBIni.UserProfileDataDB.DeleteFindAllClass();

                //reading the data

                var url = "https://kz8s1kcc7j.execute-api.ap-southeast-2.amazonaws.com/serena/getproclassforall";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {

                                    var proMember = new FindUploadClassModel();

                                    double rating = 0;
                                    dynamic item11 = item.AvgRating;
                                    if (item11 == "" || item11 == null)
                                        rating = 0;
                                    else
                                        rating = Math.Round(Convert.ToDouble(item11), 1);


                                    proMember.Guid = item.Guid;
                                    proMember.UserEmailId = item.UserEmailId;
                                    proMember.MediaUrl = item.MediaUrl;
                                    proMember.CoverPhotoUrl = item.CoverPhotoUrl;
                                    proMember.Title = item.Title;
                                    proMember.About = item.About;
                                    proMember.PlayList = "5";
                                    proMember.Category = item.Category;
                                    proMember.Tags = item.Tags;
                                    proMember.Mentions = item.Mentions == "" ? "EASY" : item.Mentions;
                                    proMember.Duration = item.Duration;
                                    proMember.AvgRating = rating;
                                    proMember.TotalReviews = item.TotalReviews;

                                    App.DBIni.UserProfileDataDB.AddFindClass(proMember);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetProclassForAll";

            }
        }

        public async Task GetSerenaExploreTopHasTagCount()
        {
            try
            {
                #region deleting previous trending each time

                App.DBIni.TophashtagDB.DeleteTophashtag();//delete all records from Tophashtags
                App.DBIni.TophashtagDB.DeleteSearchTophashtag();//delete all records from SearchHashTag

                #endregion

                var url = "https://fkk3rybfna.execute-api.ap-southeast-2.amazonaws.com/serena/gettophashtagscnt";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {
                                    var tophashTags = new Tophashtags();

                                    tophashTags.Hashtags = item.Hashtags;
                                    tophashTags.Count = item.Count;
                                    tophashTags.Img1 = item.Img1;
                                    tophashTags.Img2 = item.Img2;
                                    tophashTags.Img3 = item.Img3;

                                    App.DBIni.TophashtagDB.AddTophashtag(tophashTags);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        public async Task GetProSocial(string userEmailId)
        {
            try
            {
                //deleteing all social profile from session user
                //App.DBIni.UserProfileDataDB.DeleteAllSocial();

                var payload = new Dictionary<string, string>
                        {
                         {"UserEmailId",   userEmailId},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://4apm2vxav3.execute-api.ap-southeast-2.amazonaws.com/serena/getprosocial";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                var proMember = new ProMemberToolsModel();

                                proMember.UserEmailId = item.UserEmailId;
                                proMember.Categories = item.Categories;
                                proMember.Tagline = item.Tagline;
                                proMember.About = item.About;
                                proMember.Coverphoto = item.Coverphoto;

                                App.DBIni.UserProfileDataDB.AddPromemberTool_Social(proMember);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }
        #endregion

        #region Users data

        public async Task GetUsersdataAsync()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://2uhfsh20u2.execute-api.ap-southeast-2.amazonaws.com/serena/getuserslistprofilepage";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                UserProfileData userProfileData = new UserProfileData();

                                userProfileData.UserEmailId = item.UserEmailId;
                                userProfileData.UserPostJson = item.UserPostJson;
                                userProfileData.UserProfileImage = item.UserProfileImage;
                                userProfileData.UserAccountType = item.UserAccountType;
                                userProfileData.UserBio = item.UserBio;
                                userProfileData.UserWebsite = item.UserWebsite;
                                userProfileData.UserLocation = item.UserLocation;
                                userProfileData.UserAgeRange = item.UserAgeRange;
                                userProfileData.UserName = item.UserName;

                                userProfileData.ContactPhone = item.ContactPhone;
                                userProfileData.ContactEmail = item.ContactEmail;
                                userProfileData.AllowContact = item.AllowContact;
                                userProfileData.IsPro = item.IsPro;
                                var retrunvalue = App.DBIni.UserProfileDataDB.AddUser(userProfileData);

                                UserProfileData_Tran userProileDataTran = new UserProfileData_Tran();

                                userProileDataTran.UserEmailId = item.UserEmailId;
                                userProileDataTran.Status = item.TrequestStatus;
                                userProileDataTran.Guid = item.Guid;
                                var retrunvalue1 = App.DBIni.UserProfileDataTranDB.AddUser(userProileDataTran);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetUsersdataAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaUsersAsync()
        {
            try
            {
                var url = "https://n1f29fg8qk.execute-api.ap-southeast-2.amazonaws.com/serena/getallserenauserrds";
                var client = new HttpClient();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                SignUpModel sm = new SignUpModel();

                                sm.FULLNAME = item.FullName;
                                sm.EMAIL = item.EMAIL;
                                sm.LastCreated = item.LastCreated;
                                sm.LastLoginTime = item.LastLoginTime;
                                sm.LastUpdated = item.LastUpdated;
                                sm.PASSWORD = item.Password;
                                sm.PLACE = item.Place;
                                sm.UserCategory = item.UserCategory;
                                sm.UserLoginType = item.UserLoginType;
                                sm.USERNAME = item.UserName;
                                App.DBIni.SignUpDB.AddUser(sm);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaUsersAsync";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                //   logFile.AddExceptionLog(addlogFile);
            }
        }

        #endregion

        #region Add Login, SignUp, UserProfile data
        public async Task SaveSerenaUserData(SignUpModel_Out signUp)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"EMAIL", signUp.EMAIL},
                        {"Password", signUp.PASSWORD},
                        {"FullName", signUp.FULLNAME},
                        {"UserName", signUp.USERNAME},
                        {"Place", signUp.PLACE},
                        {"UserCategory", signUp.UserCategory},
                        {"UserLoginType", signUp.UserLoginType},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://819hbsml6h.execute-api.ap-southeast-2.amazonaws.com/serena/saveserenauserrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }

        public async Task PostUpdatedPassword(string EmailId, string Password)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Password", Password},
                        {"EMAIL", EmailId},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://0xehe1f0ve.execute-api.ap-southeast-2.amazonaws.com/serena/updatepassword";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task SaveSerenaUserProfileData(UserProfileData_Out profileData)
        {
            try
            {
                var payload = new Dictionary<string, string>
                            {
                            {"UserEmailId", profileData.UserEmailId},
                            {"UserAccountType", "PUBLIC"},
                            {"UserName", profileData.UserName},
                            };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://mooeqagtlh.execute-api.ap-southeast-2.amazonaws.com/serena/insertuserprofilerds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }

        public async Task UpdateUserProfileData(UserProfileData_Out uProfileData)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", uProfileData.UserEmailId},
                    {"UserProfileImage", uProfileData.UserProfileImage},
                    {"UserAccountType", uProfileData.UserAccountType},
                    {"UserBio", uProfileData.UserBio},
                    {"UserWebsite", uProfileData.UserWebsite},
                    {"UserLocation", uProfileData.UserLocation},
                    {"UserAgeRange", uProfileData.UserAgeRange},
                    {"UserName", uProfileData.UserName}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://grh01wkgr9.execute-api.ap-southeast-2.amazonaws.com/serena/updateuserprofilerds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }
        #endregion

        #region Post data
        //public async Task GetSerenaPostAsync()
        //{
        //    try
        //    {
        //        var payload = new Dictionary<string, string>
        //                {
        //                {"UserEmailId", sessionEmailId}
        //                };
        //        var serializeJson = JsonConvert.SerializeObject(payload);
        //        var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

        //        var url = "https://s8kjad0jw5.execute-api.ap-southeast-2.amazonaws.com/serena/getallserenapostrds";
        //        var client = new HttpClient();

        //        var response = await client.PostAsync(url, content);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            string result = response.Content.ReadAsStringAsync().Result;
        //            var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
        //            foreach (var getBody in deserializeJson)
        //            {
        //                if (getBody.Key == "body")
        //                {
        //                    string getValue = getBody.Value.ToString();
        //                    string replaceData1 = getValue.Replace("\n", "");

        //                    dynamic PL = JArray.Parse(replaceData1);

        //                    foreach (var item in PL)
        //                    {
        //                        Serena_Post sm = new Serena_Post();
        //                        dynamic item14 = item.Opacity;
        //                        string Opacity = item14;

        //                        dynamic item16 = item.Longitude;
        //                        string Longitude = item16;

        //                        dynamic item17 = item.Latitude;
        //                        string Latitude = item17;

        //                        dynamic item20 = item.Favourite;
        //                        string Favourite = item20;

        //                        sm.UserEmailId = item.UserEmailId;
        //                        sm.Description = item.Description;
        //                        sm.LastCreated = item.LastCreated;
        //                        sm.ShareType = item.ShareType;
        //                        sm.LastUpdated = item.LastUpdated;
        //                        sm.Location = item.Location;
        //                        sm.Topics = item.Topics;
        //                        sm.ImageUrl = item.ImageUrl;
        //                        sm.Tags = item.Tags;
        //                        sm.HashTags = item.HashTags;
        //                        sm.LastCreated = item.LastCreated;
        //                        sm.LastUpdated = item.LastUpdated;
        //                        sm.PostText = item.PostText;
        //                        sm.PostColor = item.PostColor;
        //                        sm.PostBackgroundColor = item.BackgroundColor;
        //                        sm.Opacity = Convert.ToDouble(Opacity);
        //                        sm.PlaceAddress = item.PlaceAddress;
        //                        sm.Longitude = Convert.ToDouble(Longitude);
        //                        sm.Latitude = Convert.ToDouble(Latitude);
        //                        sm.PostType = item.PostType;
        //                        sm.Guid = item.Guid;
        //                        sm.Favourite = Convert.ToInt32(Favourite);
        //                        sm.FavCount = item.FavCount;

        //                        App.DBIni.SerenaPostDB.AddPost(sm);

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var addlogFile = new ExceptionLog();
        //        addlogFile.ExceptionDetails = ex.Message;
        //        addlogFile.ExceptionCategoryScreenName = "App.Xaml";
        //        addlogFile.ExceptionEventName = "GetSerenaPostAsync";
        //        App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
        //        //logFile.AddExceptionLog(addlogFile);
        //    }
        //}
        #endregion

        #region Notifications
        public async Task GetSerenaUpcomingNotifications()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://y9rzgdlj37.execute-api.ap-southeast-2.amazonaws.com/serena/getupcommingactivities";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Upcoming_Notifications sn = new Upcoming_Notifications();
                                sn.UserEmailId = sessionEmailId;
                                sn.ClassGuid = item.ClassGuid;
                                sn.MediaUrl = item.MediaUrl;
                                sn.CoverPhotoUrl = item.CoverPhotoUrl;
                                sn.Title = item.Title;
                                sn.About = item.About;
                                sn.Category = item.Category;
                                sn.Tags = item.Tags;
                                sn.Mentions = item.Mentions;
                                sn.Duration = item.Duration;
                                sn.WatchPartyGuid = item.WatchPartyGuid;
                                sn.ScheduledDate = item.ScheduledDate;
                                sn.ScheduledTime = item.ScheduledTime;
                                sn.AMPM = item.AMPM;
                                sn.HostEmailID = item.HostEmailID;
                                sn.Accepted = item.Accepted;
                                sn.AvgRating = item.AvgRating;
                                sn.TotalReviews = item.TotalReviews;
                                sn.Members = item.Members;

                                var notifyData = App.DBIni.NotificationDB.AddUpcomingNotifications(sn);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaUpcomingNotifications";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaNotifications()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://c0xx4vpe2i.execute-api.ap-southeast-2.amazonaws.com/serena/getnotifications";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Serena_Notifications sn = new Serena_Notifications();
                                dynamic item1 = item.CountORRequestBy;
                                string CountORRequestBy = item1;
                                sn.CountORRequestBy = CountORRequestBy;

                                dynamic item2 = item.Guid;
                                string Guid = item2;
                                sn.Guid = Guid;

                                dynamic item3 = item.ImageUrl;
                                string ImageUrl = item3;
                                sn.ImageUrl = ImageUrl;

                                dynamic item4 = item.PostText;
                                string PostText = item4;
                                sn.PostText = PostText;

                                dynamic item5 = item.PostColor;
                                string PostColor = item5;
                                sn.PostColor = PostColor;

                                dynamic item6 = item.BackgroundColor;
                                string BackgroundColor = item6;
                                sn.BackgroundColor = BackgroundColor;

                                dynamic item7 = item.PostType;
                                string PostType = item7;
                                sn.PostType = PostType;


                                dynamic item8 = item.category;
                                string category = item8;
                                sn.category = category;

                                dynamic item9 = item.UserEmailId;
                                string UserEmailId = item9;
                                sn.UserEmailId = UserEmailId;

                                dynamic item10 = item.LastUpdated;
                                string LastUpdated = item10;
                                sn.LastUpdated = LastUpdated;

                                App.DBIni.NotificationDB.AddNotifications(sn);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Notification.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaNotifications";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        #endregion

        #region schedule Watch Party

        public async Task SaveWatchParty(WatchParty_Out watchParty)
        {
            try
            {

                var serializeJson = JsonConvert.SerializeObject(watchParty);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://019fabi9p9.execute-api.ap-southeast-2.amazonaws.com/serena/inserwatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        public async Task SaveWatchPartyMembers(WatchPartyTran_Out watchPartyTrn)
        {
            try
            {

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", watchPartyTrn.WatchPartyGuid},
                        {"Members", watchPartyTrn.Members}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://id2hunla57.execute-api.ap-southeast-2.amazonaws.com/serena/insertwatchpartymembers";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        public async Task DeleteWatchParty(string WatchPartyGuid)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", WatchPartyGuid}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://opr6l6yvp9.execute-api.ap-southeast-2.amazonaws.com/serena/deletewatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "DeleteWatchParty";

            }
        }

        public async Task UpdateWatchParty(DateTime SHDate, string SHTime, string WPGuid)
        {
            try
            {
                var payload = new Dictionary<string, object>
                        {
                        {"Guid", WPGuid},
                      {"ScheduleDate", SHDate},
                      {"ScheduleTime", SHTime}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://427yvjzy46.execute-api.ap-southeast-2.amazonaws.com/serena/updatewatchpartydates";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "UpdateWatchParty";

            }
        }

        public async Task GetWatchPartyDetails(string WPGuid)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", WPGuid}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://ub2pwaj7uh.execute-api.ap-southeast-2.amazonaws.com/serena/getwatchpartydetails";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                WatchPartyTran WPT = new WatchPartyTran();
                                WPT.WatchPartyGuid = item.Guid;
                                WPT.Members = item.Members;

                                var notifyData = App.DBIni.WatchPartyDB.AddWatchPartyTran(WPT);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "GetWatchPartyDetails";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        public async Task AcceptWatchPartyNotification(string accepted, string watchPartyGuid, string members)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Accepted", accepted},
                        {"Guid", watchPartyGuid},
                        {"Members", members}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://z4jvo1rzae.execute-api.ap-southeast-2.amazonaws.com/serena/acceptserenawatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        #endregion
    }
}
