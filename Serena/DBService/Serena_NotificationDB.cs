﻿using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Serena.Interface;

namespace Serena.DBService
{
    public class Serena_NotificationDB

    {

        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb
        public Serena_NotificationDB()
        {

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _ = _SQLiteConnection.CreateTable<Serena_Notifications>();
            _ = _SQLiteConnection.CreateTable<Upcoming_Notifications>();

        }
        #region Notification
        public IEnumerable<Serena_Notifications> GetNotifications()
        {


            return (from u in _SQLiteConnection.Table<Serena_Notifications>()
                    select u).ToList();

        }
        public IEnumerable<Serena_Notifications> GetNotificationsBySessionId(string sessionEmailId)
        {


            return (from usr in _SQLiteConnection.Table<Serena_Notifications>()
                    select usr).Where(x => x.UserEmailId != sessionEmailId)
                .OrderBy(x => x.Id).ToList();

        }

        public Serena_Notifications GetSpecificNotificationsByGuid(string guid)
        {


            return (from usr in _SQLiteConnection.Table<Serena_Notifications>()
                    select usr).Where(x => x.Guid == guid)
                .OrderBy(x => x.Id).FirstOrDefault();

        }

        public string AddNotifications(Serena_Notifications notify)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                var data = _SQLiteConnection.Table<Serena_Notifications>();
                var d1 = data.Where(x => x.Guid == notify.Guid).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(notify);
                    return "Sucessfully Added";
                }
                else
                {
                    notify.Guid = d1.Guid;
                    // UpdateNotification(notify);
                    return "Already post Exist";

                }
            }
        }

        public bool UpdateNotification(Serena_Notifications notifyData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Update(notifyData);
                return true;
            }
        }


        #endregion

        #region Upcoming Notifications

        //To get list of all upcoming notifications
        public IEnumerable<Upcoming_Notifications> GetUpcomingNotifications()
        {


            return (from u in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select u).Where(x => x.ScheduledDate.Year >= DateTime.Now.Year && x.ScheduledDate.Month >= DateTime.Now.Month && x.ScheduledDate.Day >= DateTime.Now.Day).OrderByDescending(x => x.ScheduledDate).ToList();

        }
        //To get list(also count) of invitation by  status
        public IEnumerable<Upcoming_Notifications> GetUpcomingNotificationsBystatus(string accepted, string SessionEmailId)
        {


            return (from usr in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select usr).Where(x => x.Accepted == accepted && x.HostEmailID != SessionEmailId && x.ScheduledDate.Year >= DateTime.Now.Year && x.ScheduledDate.Month >= DateTime.Now.Month && x.ScheduledDate.Day >= DateTime.Now.Day)
                .OrderBy(x => x.Id).ToList();

        }
        //To get watchparty created by SessionEmailId
        public IEnumerable<Upcoming_Notifications> GetClasses(string SessionEmailId)
        {


            return (from usr in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select usr).Where(x => x.HostEmailID != SessionEmailId)
                .OrderBy(x => x.Id).ToList();

        }

        //To get latest single record to show invites
        public Upcoming_Notifications GetInviteBycategory(string accepted, string EmailId)
        {
            return (from usr in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select usr).Where(x => x.Accepted == accepted && x.UserEmailId == EmailId && x.ScheduledDate.Year >= DateTime.Now.Year && x.ScheduledDate.Month >= DateTime.Now.Month && x.ScheduledDate.Day >= DateTime.Now.Day && x.HostEmailID != EmailId)
                .OrderBy(x => x.Id).LastOrDefault();

        }

        //To get Notifications by WatchParty GUId
        public Upcoming_Notifications GetUpcomingNotificationsByGuid(string watchPartyGuid)
        {


            return (from usr in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select usr).Where(x => x.WatchPartyGuid == watchPartyGuid)
                .OrderBy(x => x.Id).FirstOrDefault();

        }

        //To get upcoming notofication by status
        public IEnumerable<Upcoming_Notifications> GetAcceptedWatchPartyDetails(string accepted, string EmailId)
        {


            return (from usr in _SQLiteConnection.Table<Upcoming_Notifications>()
                    select usr).Where(x => x.Accepted == accepted && x.UserEmailId == EmailId)
                .OrderBy(x => x.Id).ToList();

        }
        //To add Upcoming notyifications
        public string AddUpcomingNotifications(Upcoming_Notifications notify)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                var data = _SQLiteConnection.Table<Upcoming_Notifications>();
                var d1 = data.Where(x => x.WatchPartyGuid == notify.WatchPartyGuid).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(notify);
                    return "Sucessfully Added";
                }
                else
                {
                    notify.Id = d1.Id;
                    UpdateUpcomingNotifications(notify);
                    return "Already post Exist";

                }
            }
        }

        //To update Upcoming notyifications
        public bool UpdateUpcomingNotifications(Upcoming_Notifications notifyData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Update(notifyData);
                return true;
            }
        }

        public string DeleteByWPGuId(string WPGuid)
        {
            var data = _SQLiteConnection.Table<Upcoming_Notifications>();
            var d1 = data.Where(x => x.WatchPartyGuid == WPGuid).FirstOrDefault();
            if (d1 != null)
            {
                _SQLiteConnection.Delete(d1);
                return "Deleted";
            }
            else
            {
                return "Record not found!";
            }
        }
        #endregion
    }
}