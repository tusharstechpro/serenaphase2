﻿using Serena.Interface;
using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class TopicsDB
    {
        private SQLiteConnection _SQLiteConnection;

        private static object collisionLock = new object(); //amd 2nd feb


        public TopicsDB()
        {
            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _ = _SQLiteConnection.CreateTable<Topics>();

        }
        public IEnumerable<Topics> GetTopicsData()
        {
            return (from u in _SQLiteConnection.Table<Topics>()
                    select u).ToList();
        }

        public void DeleteTopics(string id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _ = _SQLiteConnection.Delete<Topics>(id);
            }
        }
        public string AddTopics(Topics top)
        {

            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<Topics>();
                var d1 = data.Where(x => x.TopicName == top.TopicName).FirstOrDefault();
                if (d1 == null)
                {
                    _ = _SQLiteConnection.Insert(top);
                    return "Sucessfully Added";
                }
                else
                {
                    top.Id = d1.Id;
                    updateTopics(top);
                    return "Topics Already Exist";
                }
            }
        }
        public bool updateTopics(Topics topData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _ = _SQLiteConnection.Update(topData);
                return true;

            }
        }

    }
}
