﻿using Serena.Interface;
using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class UserProfileDataTranDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb
        public UserProfileDataTranDB()
        {
            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<UserProfileData_Tran>();
            _SQLiteConnection.CreateTable<UserProfileDataTran_Out>();
        }

        public IEnumerable<UserProfileData_Tran> GetUsers()
        {
            return (from u in _SQLiteConnection.Table<UserProfileData_Tran>()
                    select u).ToList();
        }

        public IEnumerable<UserProfileData_Tran> GetSpecificUserByStatus(int status)
        {
            return (from usr in _SQLiteConnection.Table<UserProfileData_Tran>()
                    select usr).Where(x => x.Status == status)
                    .OrderBy(x =>
                    {
                        return x.UserTranId;
                    }).ToList();
        }

        public UserProfileData_Tran GetGuidByUserEmailId(string userEmailId)
        {
            return _SQLiteConnection.Table<UserProfileData_Tran>().FirstOrDefault(t => t.UserEmailId == userEmailId);
        }

        public string AddUser(UserProfileData_Tran profile_tran)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<UserProfileData_Tran>();
                var d1 = data.Where(x => x.UserEmailId == profile_tran.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(profile_tran);
                    return "Sucessfully Added";
                }
                else
                {
                    profile_tran.UserTranId = d1.UserTranId;
                    updateUsers(profile_tran);
                    return "Already Mail id Exist";

                }
            }
        }

        public string AddOutUser(UserProfileDataTran_Out profile_tranout)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<UserProfileDataTran_Out>();
                var d1 = data.Where(x => x.UserEmailId == profile_tranout.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(profile_tranout);
                    return "Sucessfully Added";
                }
                else
                {
                    profile_tranout.UserTranOutId = d1.UserTranOutId;
                    updateUsers(profile_tranout);
                    return "Already Mail id Exist";

                }
            }
        }

        public bool updateUsers(UserProfileDataTran_Out profiletranoutdata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(profiletranoutdata);
                return true;
            }
        }

        public bool updateUsers(UserProfileData_Tran profiletrandata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(profiletrandata);
                return true;
            }
        }

        //to get UserProfileData_Tran  data between time interval --- Follow button clicked
        public IEnumerable<UserProfileDataTran_Out> GetAllUserProfileData_TranOut(DateTime out_sync_date, int Status)
        {
            return (from u in _SQLiteConnection.Table<UserProfileDataTran_Out>()
                    select u).Where(x => x.LastSyncDate > out_sync_date && x.Status == Status).ToList();
        }

        public IEnumerable<SignUpModel_Out> GetData()
        {
            return (from u in _SQLiteConnection.Table<SignUpModel_Out>()
                    select u).ToList();
        }

    }
}