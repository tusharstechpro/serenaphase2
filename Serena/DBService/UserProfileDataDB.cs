﻿using Serena.Interface;
using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class UserProfileDataDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb
        public UserProfileDataDB()
        {

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<UserProfileData>();
            _SQLiteConnection.CreateTable<ProMemberToolsModel>();
            _SQLiteConnection.CreateTable<UploadMediaModel>();
            _SQLiteConnection.CreateTable<FindUploadClassModel>();
            _SQLiteConnection.CreateTable<SearchClassModel>();
            _SQLiteConnection.CreateTable<UserProfileData_Out>();
            _SQLiteConnection.CreateTable<UploadMediaModel_Out>();

        }

        #region UserProfileData
        public IEnumerable<UserProfileData> GetUsers()
        {

            return (from u in _SQLiteConnection.Table<UserProfileData>()
                    select u).ToList();

        }

        public IEnumerable<UserProfileData> GetAllUserProfile(string sessionEmailId)
        {

            return (from usr in _SQLiteConnection.Table<UserProfileData>()
                    select usr).Where(x => x.UserEmailId != sessionEmailId)
                .OrderBy(x => x.Id).ToList();

        }

        public UserProfileData GetSpecificUser(string id)
        {

            return _SQLiteConnection.Table<UserProfileData>().FirstOrDefault(t => t.UserEmailId == id);

        }

        public UserProfileData GetSpecificUserByUserName(string UserName)
        {
            return _SQLiteConnection.Table<UserProfileData>().FirstOrDefault(t => t.UserName == UserName);

        }

        public void DeleteUser(string id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Delete<UserProfileData>(id);
            }
        }
        public string AddUser(UserProfileData profile)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                var data = _SQLiteConnection.Table<UserProfileData>();
                var d1 = data.Where(x => x.UserEmailId == profile.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(profile);
                    return "Sucessfully Added";
                }
                else
                {
                    profile.Id = d1.Id;
                    updateUsers(profile);
                    return "Already Mail id Exist";

                }
            }
        }
        public bool updateUsers(UserProfileData profiledata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Update(profiledata);
                return true;
            }
        }

        #endregion

        #region UserProfileData_Out
        public string AddUserOut(UserProfileData_Out profileout)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Insert(profileout);
                return "Sucessfully Added";
            }
        }

        public IEnumerable<UserProfileData_Out> GetAllUPData_Out(DateTime out_sync_date, string status)
        {
            return (from u in _SQLiteConnection.Table<UserProfileData_Out>()
                    select u).Where(x => x.LastSyncDate > out_sync_date && x.Status == status).ToList();

        }
        #endregion

        #region ProMemberToolsModel
        public ProMemberToolsModel GetPromemberTool_Social(string sessionEmailId)
        {
            return (from usr in _SQLiteConnection.Table<ProMemberToolsModel>()
                    select usr).Where(x => x.UserEmailId == sessionEmailId)
                   .OrderBy(x => x.Id).FirstOrDefault();

        }
        public string AddPromemberTool_Social(ProMemberToolsModel profile)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<ProMemberToolsModel>();
                var d1 = data.Where(x => x.UserEmailId == profile.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(profile);
                    return "Sucessfully Added";
                }
                else
                {
                    profile.Id = d1.Id;
                    updatePromemberTool_Social(profile);
                    return "Already Mail id Exist";

                }
            }
        }
        //Get all users except loggedin user  

        public bool updatePromemberTool_Social(ProMemberToolsModel profiledata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(profiledata);
                return true;
            }
        }
        public void DeleteAllSocial()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.DeleteAll<ProMemberToolsModel>();
            }
        }
        #endregion

        #region upload media model

        public void DeleteAllClass()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.DeleteAll<UploadMediaModel>();
            }
        }
        public string AddProClass(UploadMediaModel updateProClass)
        {
            //var data = _SQLiteConnection.Table<UploadMediaModel>();
            //var d1 = data.Where(x => x.UserEmailId == updateProClass.UserEmailId).FirstOrDefault();
            //if (d1 == null)
            //{
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Insert(updateProClass);
                return "Sucessfully Added";
            }
            //}
            //else
            //{
            //    updateProClass.Id = d1.Id;
            //    UpdateProClass(updateProClass);
            //    return "Already Mail id Exist";

            //}
        }
        public bool UpdateProClass(UploadMediaModel profiledata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Update(profiledata);
                return true;
            }
        }
        public IEnumerable<UploadMediaModel> GetProClassDetailsByEmailId(string sessionEmailId)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                return (from usr in _SQLiteConnection.Table<UploadMediaModel>()
                        select usr).Where(x => x.UserEmailId == sessionEmailId)
                    .OrderBy(x => x.Id).ToList();
            }
        }

        #endregion

        #region UploadMediaModelOut
        public string AddProClass_Out(UploadMediaModel_Out updateProClass)
        {

            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Insert(updateProClass);
                return "Sucessfully Added";
            }

        }
        public IEnumerable<UploadMediaModel_Out> GetProClass_Out(DateTime out_sync_date)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                return (from u in _SQLiteConnection.Table<UploadMediaModel_Out>()
                        select u).Where(x => x.LastSyncDate > out_sync_date).ToList();//pending
            }
        }

        #endregion

        #region find  class

        public void DeleteFindAllClass()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.DeleteAll<FindUploadClassModel>();
            }
        }
        public string AddFindClass(FindUploadClassModel updateProClass)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                var classes = (from cls in _SQLiteConnection.Table<FindUploadClassModel>()
                               select cls).Where(x => x.Guid == updateProClass.Guid).FirstOrDefault();
                if (classes == null)
                {
                    _SQLiteConnection.Insert(updateProClass);
                    return "Sucessfully Added";
                }
                else
                {
                    updateProClass.Id = classes.Id;
                    _SQLiteConnection.Update(updateProClass);

                    return "Sucessfully Added";
                }
            }

        }
        public bool UpdateFindClass(FindUploadClassModel profiledata)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Update(profiledata);
                return true;
            }
        }
        //public IEnumerable<FindUploadClassModel> GetTopFindClass()
        //{


        //    return (from u in _SQLiteConnection.Table<FindUploadClassModel>()
        //            select u).ToList();

        //}
        public FindUploadClassModel GetTopFindClassByGuid(string seletectdGuid)
        {

            return (from usr in _SQLiteConnection.Table<FindUploadClassModel>()
                    select usr).Where(x => x.Guid == seletectdGuid)
                .OrderBy(x => x.Id).FirstOrDefault();

        }

        // top search class

        public void DeleteSearchAllClass()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.DeleteAll<SearchClassModel>();
            }
        }
        public string AddSearchClass(SearchClassModel addProClass)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Insert(addProClass);
                return "Sucessfully Added";
            }

        }
        public IEnumerable<SearchClassModel> GetSearchTopClassTitle()
        {

            return (from u in _SQLiteConnection.Table<SearchClassModel>()
                    select u).ToList();

        }
        public IEnumerable<FindUploadClassModel> GetTopFindClassByCategory(string selectedCategory)
        {

            return (from u in _SQLiteConnection.Table<FindUploadClassModel>()
                    select u).Where(x => x.Category.ToLower() == selectedCategory)
                .OrderBy(x => x.Id).ToList();

        }

        public IEnumerable<FindUploadClassModel> GetTopFindClass()
        {
            return (from u in _SQLiteConnection.Table<FindUploadClassModel>()
                    select u).OrderByDescending(x => x.AvgRating).ToList();

        }
        #endregion
    }
}