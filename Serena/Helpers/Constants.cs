﻿
namespace Serena.Helpers
{
    public static class Constants
    {
        //https://www.facebook.com/v7.0/dialog/oauth?client_id=2681294068755579&response_type=token&redirect_uri=https://www.facebook.com/connect/login_success.html

        public static string AppName = "Serena";
        //public static string GAppName = "Serena Google Project";

        // Created Client id and url from admin.techpro.co.in account under serena google project *********************

        // Google OAuth Serena project

        // For Google login, configure at https://console.developers.google.com/
        public static string GoogleiOSClientId = "117882128924-ffmuvmn5lt09t0mhm6q59nsipjncqa6v.apps.googleusercontent.com";
        public static string GoogleAndroidClientId = "117882128924-qki47n95ms1jcrrpklcpi8o0me4p94ug.apps.googleusercontent.com";

        // shraddha code 
        //public static string GoogleiOSClientId = "140593368088-7084m3kbde00cetmul18ung5hlq1mlb6.apps.googleusercontent.com";
        //public static string GoogleAndroidClientId = "140593368088-gfr4r9mejleo993av4g0830oueklvap4.apps.googleusercontent.com";

        // These values do not need changing
        public static string GoogleScope = "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile";
        public static string GoogleAuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string GoogleAccessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
        public static string GoogleUserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";

        //shraddha code
        //public static string GoogleiOSRedirectUrl = "com.googleusercontent.apps.140593368088-7084m3kbde00cetmul18ung5hlq1mlb6:/oauth2redirect";
        //public static string GoogleAndroidRedirectUrl = "com.googleusercontent.apps.140593368088-gfr4r9mejleo993av4g0830oueklvap4:/oauth2redirect";

        // Set these to reversed iOS/Android client ids, with :/oauth2redirect appended
        public static string GoogleiOSRedirectUrl = "com.googleusercontent.apps.117882128924-ffmuvmn5lt09t0mhm6q59nsipjncqa6v:/oauth2redirect";
        public static string GoogleAndroidRedirectUrl = "com.googleusercontent.apps.117882128924-qki47n95ms1jcrrpklcpi8o0me4p94ug:/oauth2redirect";

        //-------------------------------------------------------------------------------------------------------

        // Facebook OAuth from shraddha account
        // For Facebook login, configure at https://developers.facebook.com
        //public static string FacebookiOSClientId = "288250585863562";
        //public static string FacebookAndroidClientId = "288250585863562";

        public static string FacebookiOSClientId = "1177099239330610";
        public static string FacebookAndroidClientId = "1177099239330610";


        // These values do not need changing
        public static string FacebookScope = "email";
        public static string FacebookAuthorizeUrl = "https://www.facebook.com/dialog/oauth/";
        public static string FacebookAccessTokenUrl = "https://www.facebook.com/connect/login_success.html";
        public static string FacebookUserInfoUrl = "https://graph.facebook.com/me?fields=email&access_token={accessToken}";

        // Set these to reversed iOS/Android client ids, with :/oauth2redirect appended
        public static string FacebookiOSRedirectUrl = "https://www.facebook.com/connect/login_success.html";
        public static string FacebookAndroidRedirectUrl = "https://www.facebook.com/connect/login_success.html";

        //-------------------------------------------------------------------------------------------------------

        //GoogleMaps API Key
        public const string GoogleMapsApiKey = "AIzaSyDr4w8VKpxUqf_nxVKxGZBtMjHPff-L_fg";
        //GooglePlaces API Key
        public const string GooglePlacesApiKey = "AIzaSyBmiKqeg_qdqiW4idQkAyHHAt9hvJ-XYmc";
        //GooglePlacesApiAutoCompletePath URL
        public const string GooglePlacesApiAutoCompletePath = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key={0}&input={1}";  //Adding country:us limits results to us

        //S3 Credentials                                                                                                                                       //GooglePlacesApiKey
        //public const string ProfilePagebucketName = "profilessimages";
        //public const string PostbucketName = "serenapostimages";
        //public static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;
       // public static IAmazonS3 client;

        public const string _accessKey = "AKIAX26PMQIRFGLI6ZXE";
        public const string _secretKey = "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";

        //-----------------------------Plugin.Toast--------------------------------------------------------------------------
        public static string ToastMessage = "Your device is not connected to the internet.";
        public static string BgColor = "#9900CC";
        public static string TextColor = "#F4F4F4";
    }
}
