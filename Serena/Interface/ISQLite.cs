﻿using SQLite;

namespace Serena.Interface
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}