﻿using Serena.Model.SocialIntegration;
using System.Threading.Tasks;

namespace Serena.Interface
{
    public interface IAppleSignInService
    {
        bool IsAvailable { get; }
        Task<AppleSignInCredentialState> GetCredentialStateAsync(string userId);
        Task<AppleAccount> SignInAsync();
    }
}
