﻿using Serena.Model;
using System;
using System.Collections.Generic;
using System.Text;


namespace Serena.Interface
{
    public interface ICalenderDroid
    {
        //To add event in android
        void RID();
        void AddEventInAndroid(List<ScheduleDateTime> SDT);

        //To add event in iOS
        void AddEventIniOS(List<ScheduleDateTime> SDT);
    }
}
