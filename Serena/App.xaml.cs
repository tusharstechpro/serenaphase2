﻿using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Serena.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Matcha.BackgroundService;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Serena.Interface;
using Serena.Model.SocialIntegration;

[assembly: ExportFont("Cabin.ttf")]
namespace Serena
{
    public partial class App : Application
    {
        public static DBInitialization DBIni = new DBInitialization();
        public static string FilePath;
        string sessionEmailId;
        //For signinwithapple..test
        public const string LoggedInKey = "LoggedIn";
        public const string AppleUserIdKey = "AppleUserIdKey";
        string userId;
        public App()
        {
            InitializeComponent();
            //Get the login user EmailId from Session
            sessionEmailId = Settings.GeneralSettings;
            MainPage = new SerenaTabbedPage();
        }

        protected async override void OnStart()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    if (string.IsNullOrEmpty(sessionEmailId))
                    {
                        #region add data in lastsync initially
                        LastSync ls = new LastSync();
                        ls.TableName = "WatchParty_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returns = App.DBIni.AgeDB.AddLastSync(ls);//1st row

                        ls.TableName = "WatchPartyTran_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returnss = App.DBIni.AgeDB.AddLastSync(ls);//2nd row

                        ls.TableName = "UserProfileDataTran_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returnsUserProfileDataTran_Out = App.DBIni.AgeDB.AddLastSync(ls);//3rd row

                        ls.TableName = "UserProfileData_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returnsUserProfileData_Out = App.DBIni.AgeDB.AddLastSync(ls);//4th row

                        ls.TableName = "UploadMediaModel_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returnUploadMediaModel_Out = App.DBIni.AgeDB.AddLastSync(ls);//5th row


                        ls.TableName = "SerenaReviews_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var returnSerenaReviews_Out = App.DBIni.AgeDB.AddLastSync(ls);//6th row

                        ls.TableName = "SignUpModel_Out";
                        ls.Out_last_sync = DateTime.Now;
                        ls.Get_last_sync = DateTime.Now;
                        var SignUpModel_Out = App.DBIni.AgeDB.AddLastSync(ls);//7th row

                        #endregion

                    }
                    //added by shraddhap for apple signin on 8march2021
                    #region SignInWithApple
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        var appleSignInService = DependencyService.Get<IAppleSignInService>();
                        if (appleSignInService != null)
                        {
                            userId = await SecureStorage.GetAsync(AppleUserIdKey);
                            if (appleSignInService.IsAvailable && !string.IsNullOrEmpty(userId))
                            {

                                var credentialState = await appleSignInService.GetCredentialStateAsync(userId);

                                switch (credentialState)
                                {
                                    case AppleSignInCredentialState.Authorized:
                                        //Normal app workflow...
                                        break;
                                    case AppleSignInCredentialState.NotFound:
                                    case AppleSignInCredentialState.Revoked:
                                        //Logout;
                                        SecureStorage.Remove(AppleUserIdKey);
                                        Preferences.Set(LoggedInKey, false);
                                        MainPage = new NavigationPage(new Views.SerenaTabbedPage());
                                        break;
                                }
                            }
                        }
                    }
                    #endregion
                    StartBackgroundService();
                    StartBackgroundService_Out();
                }
                // added amd 4feb
                Plugin.Permissions.Abstractions.PermissionStatus Locationstatus = await Plugin.Permissions.CrossPermissions.Current.RequestPermissionAsync<Plugin.Permissions.LocationPermission>();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "Init";

                DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        private static void StartBackgroundService()
        {
            BackgroundAggregatorService.Add(() => new PeriodicTask(120)); // 2 min refresh interval for sync
            try
            {
                //run task in the background
                BackgroundAggregatorService.StartBackgroundService();
            }
            catch (Exception)
            { }

        }

        private static void StartBackgroundService_Out()
        {
            BackgroundAggregatorService.Add(() => new PeriodicTask_Out(900)); // 15 min refresh interval for sync
            try
            {
                //run task in the background
                BackgroundAggregatorService.StartBackgroundService();
            }
            catch (Exception)
            { }

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
