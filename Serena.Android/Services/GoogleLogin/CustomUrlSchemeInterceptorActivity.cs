﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Serena.Model.SocialIntegration;

namespace Serena.Droid.Services.GoogleLogin
{
	[Activity(Label = "CustomUrlSchemeInterceptorActivity", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
	[IntentFilter(
		new[] { Intent.ActionView },
		Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
		//DataSchemes = new[] { "com.googleusercontent.apps.140593368088-gfr4r9mejleo993av4g0830oueklvap4" },shraddha gmail
		DataSchemes = new[] { "com.googleusercontent.apps.117882128924-qki47n95ms1jcrrpklcpi8o0me4p94ug" },
		DataPath = "/oauth2redirect")]
	public class CustomUrlSchemeInterceptorActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Convert Android.Net.Url to Uri
			var uri = new Uri(Intent.Data.ToString());

			// Load redirectUrl page
			AuthenticationState.Authenticator.OnPageLoading(uri);
			var intent = new Intent(this, typeof(MainActivity));
			intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
			StartActivity(intent);

			this.Finish();

			return;
		}
	}
}