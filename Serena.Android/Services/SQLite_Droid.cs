﻿using System.IO;
using Serena.Droid.Services;
using Serena.Interface;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Droid))]
namespace Serena.Droid.Services
{
    public class SQLite_Droid : ISQLite
    {
        public static SQLiteConnection con;
        public SQLiteConnection GetConnection()
        {
            if (con == null)
            {
                var dbName = "SerenaDB.db";
                var dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
                var path = Path.Combine(dbPath, dbName);
                con = new SQLiteConnection(path);
                return con;
            }
            else
            {
                return con;
            }
        }
    }
}