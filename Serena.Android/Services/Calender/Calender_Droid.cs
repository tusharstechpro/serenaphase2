﻿using Serena.Droid;
using Xamarin.Forms;
using Android.Content;
using System;
using Android.Provider;
using Java.Util;
using static Android.Provider.CalendarContract;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Serena.Model;
using SQLite;
using Serena.Interface;
using Serena.Droid.Services.Calender;

[assembly: Dependency(typeof(Calender_Droid))]
namespace Serena.Droid.Services.Calender
{
    public class Calender_Droid : ICalenderDroid
    {

        public int _calId;

        [Obsolete]
        //Method  to get Calendar ID
        public void RID()
        {
            // List Calendars
            var calendarsUri = CalendarContract.Calendars.ContentUri;

            string[] calendarsProjection = {
                        CalendarContract.Calendars.InterfaceConsts.Id,
                        CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName
                       // CalendarContract.Calendars.InterfaceConsts.AccountName
                    };
            var calCursor = Forms.Context.ContentResolver.Query(
           Calendars.ContentUri,
        calendarsProjection,
            Calendars.InterfaceConsts.Visible + " =1 AND " + Calendars.InterfaceConsts.IsPrimary + "=1",
           null,
    Calendars.InterfaceConsts.Id + " ASC"
           );
            if (calCursor != null && calCursor.Count <= 0)
            {
                calCursor = Forms.Context.ContentResolver.Query(
                    Calendars.ContentUri,
                    calendarsProjection,
                    Calendars.InterfaceConsts.Visible + " = 1",
                    null,
                    Calendars.InterfaceConsts.Id + " ASC"
                );
            }

            if (calCursor != null)
            {
                if (calCursor.MoveToPosition(1))
                {
                    var calName = "";
                    var calID = 0;
                    var nameCol = calCursor.GetColumnIndex(calendarsProjection[1]);
                    var idCol = calCursor.GetColumnIndex(calendarsProjection[0]);

                    calName = calCursor.GetString(nameCol);
                    calID = calCursor.GetInt(idCol);
                    calCursor.Close();
                    _calId = calID;
                }
            }

        }
        //  [Obsolete]
        //add To calendar method
        public void AddEventInAndroid(List<ScheduleDateTime> SDT)
        {
            var uri = CalendarContract.Calendars.ContentUri;
            ContentValues eventValues = new ContentValues();

            eventValues.Put(CalendarContract.Events.InterfaceConsts.CalendarId, _calId);
            //Event Tittle
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Title, SDT[0].Title);
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Description, "Event Created for Testing");
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtstart, GetDateTimeMS(SDT[0].Year, SDT[0].Month, SDT[0].Date, SDT[0].Hour, SDT[0].Minute));
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtend, GetDateTimeMS(SDT[0].Year, SDT[0].Month, SDT[0].Date, SDT[0].Hour, SDT[0].Minute));


            eventValues.Put(CalendarContract.Events.InterfaceConsts.EventTimezone, "UTC");
            eventValues.Put(CalendarContract.Events.InterfaceConsts.EventEndTimezone, "UTC");

            Forms.Context.ContentResolver.Insert(CalendarContract.Events.ContentUri, eventValues);
        }

        long GetDateTimeMS(int yr, int month, int day, int hr, int min)
        {
            Calendar c = Calendar.GetInstance(Java.Util.TimeZone.Default);

            if (month == 1)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.January);
            }
            else if (month == 2)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.February);
            }
            else if (month == 3)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.March);
            }
            else if (month == 4)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.April);
            }
            else if (month == 5)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.May);
            }
            else if (month == 6)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.June);
            }
            else if (month == 7)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.July);
            }
            else if (month == 8)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.August);
            }
            else if (month == 9)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.September);
            }
            else if (month == 10)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.October);
            }
            else if (month == 11)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.November);
            }
            else if (month == 12)
            {
                c.Set(Java.Util.CalendarField.Month, Calendar.December);
            }
            c.Set(Java.Util.CalendarField.DayOfMonth, day);
            c.Set(Java.Util.CalendarField.HourOfDay, hr);
            c.Set(Java.Util.CalendarField.Minute, min);
            c.Set(Java.Util.CalendarField.Year, yr);

            return c.TimeInMillis;
        }
        public void AddEventIniOS(List<ScheduleDateTime> SDT)
        { }
    }

}
